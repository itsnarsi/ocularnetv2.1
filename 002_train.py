# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-26T21:39:11-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-29T16:24:16-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer
from twrap.transform import ToZNorm, RandomBlur, RandomNoise, RandomResize
from utils.trainer_MI_SIAMESE import fit_model
from utils.models import *
from utils.stn_models import *
from utils.datasets import dataset_visob_siamese, RandomSiameseSampler
from utils.metrics import *
from utils.lr_finder import LRFinder, CyclicLR

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

logging_dir = '/media/cibitaw1/DATA/SUMMER2019/OCULARNETV2.1/EXTEND01'
model_instance_name = 'STNMODEL_4x_V200.ALL_WIN'#RFe : robust feature extarction

batch_size = 24
nb_epochs = 300
base_lr = 1e-2

transform=transforms.Compose([
    transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5),
    transforms.Grayscale(),
    transforms.RandomRotation(20),
    transforms.Resize(190),
    transforms.RandomCrop(180),
    RandomBlur(min_scale = 0.0, max_scale = 1.5),
    RandomResize(min_scale = 0.8, max_scale = 1.8),
    # transforms.RandomResizedCrop(size=160, scale=(0.8, 1.4), ratio=(0.95, 1.05)),
    transforms.CenterCrop(160),
    # ToZNorm()
    transforms.ToTensor(),
    # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])

# img_fldrs = ['/media/cibitaw1/datasets/OcularNetV2/MITGaze/img_pars.csv']
# train_dataset = dataset(img_fldrs, transform=transform)

img_fldrs = ['/media/cibitaw1/datasets/OcularNetV2/visob/img_pars_ex.csv']
other_imgs= None
# ['/media/cibitaw1/datasets/EXTENDED/casia.csv',
#              '/media/cibitaw1/datasets/EXTENDED/oulu.csv',
#              '/media/cibitaw1/datasets/EXTENDED/replaymob.csv',
#              '/media/cibitaw1/datasets/EXTENDED/siw.csv']
train_dataset = dataset_visob_siamese(img_fldrs, other_dataset = other_imgs, transform=transform, visit = 1, set_200 = True, session1 = False)
siamese_sampler = RandomSiameseSampler(train_dataset.img_pars, list(train_dataset.targets.keys()), batch_size = batch_size, num_iters = 1500)

# Training data information
train_batches = len(train_dataset)//batch_size
train_data_loader = DataLoader(train_dataset, batch_size=batch_size, sampler=siamese_sampler, num_workers=8, pin_memory=True)

model = STNMODEL_4x(illum_inv = True)
model_summary([1, 160, 160], model)

params = [{'params': parfilter(model.LF)},
          {'params': parfilter(model.LM)},
          {'params': parfilter(model.LM2)},
          {'params': parfilter(model.LW), 'lr' : 1e-4},
          {'params': parfilter(model.STN), 'lr' : 1e-4,'weight_decay' : 0.0}#
          ]

optimizer = torch.optim.SGD(params, base_lr, momentum=0.9, weight_decay=0.0001)
criterion = SphereLoss_local(in_feats=model.embed_feat, n_classes=train_dataset.num_targets, scale = 24).cuda()

# criterion = AdaCos(in_feats=model.embed_feat, n_classes=train_dataset.num_targets)
# criterion.use_s_cos = False
# lr_finder = LRFinder(model, optimizer, criterion, device="cuda")
# lr_finder.range_test(train_data_loader, end_lr=100, num_iter=500)
# lr_finder.plot()
# exit()

model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = criterion, batch_size = batch_size,
                  num_epochs = nb_epochs, scheduler_steps = [2, 5, 10, 25, 50, 100],
                  log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = True)
