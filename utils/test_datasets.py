# @Author: Narsi Reddy <cibitaw1>
# @Date:   2019-10-31T11:19:50-05:00
# @Email:  sainarsireddy@outlook.com
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-31T15:18:52-05:00
import os
from PIL import Image
import torch
torch.manual_seed(29)
from torch.utils.data import Dataset
from torch.utils.data.sampler import Sampler
import pandas as pd
import numpy as np
from glob import glob
np.random.seed(29)
from .dataset_utils import VISOB_200_TRAIN
from tqdm import tqdm



class casia_iris_twins(Dataset):
    def __init__(self, imgs_fldr, img_type = '.jpg', transform = None):

        self.imgs = glob(imgs_fldr + os.sep + '*' + os.sep + '*' + os.sep + img_type)
        self.num_samples = len(self.imgs)
        self.transform = transform

    def __getitem__(self, i):

        I = Image.open(self.imgs[i]).resize((160, 160), Image.BICUBIC)
        if self.imgs[i].split(os.sep)[-2][1]  == 'R':
            I = I.transpose(Image.FLIP_LEFT_RIGHT)
        if self.transform:
            I = self.transform(I)

        id = self.imgs[i].split(os.sep)[-3]
        eye = self.imgs[i].split(os.sep)[-2]

        return I, id+'_'+eye

    def __len__(self):
        return self.num_samples
