# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T20:32:22-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-09-17T10:05:11-05:00
import torch
torch.manual_seed(29)
from torch import nn
from twrap import layers as L
import torch.nn.functional as F
from torchvision import models as preTmodels

from .model_utils import *


class mobilenetv2_E1(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False,illum_inv = True):
        super(mobilenetv2_E1, self).__init__()
        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        R = preTmodels.mobilenet_v2(pretrained=True)
        R = list(list(R.children())[0].children())[1:-8]

        R[-4].conv[1][0].stride = (1,1)

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 2),
                                *R,
                                nn.Conv2d(in_channels=64, out_channels=256, kernel_size=1, bias=False)) #20x20

        self.ATTEN = nn.Sequential(nn.Conv2d(in_channels=256, out_channels=256, kernel_size=1, bias=False),
                                   nn.ReLU(True),
                                   nn.Conv2d(in_channels=256, out_channels=1, kernel_size=1, bias=False),
                                   nn.Softplus())

        self.embed_feat = 256
        del R

    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)

        xa = self.ATTEN(x)

        return x, xa

    def forward(self, x):

        x, xa = self.getLocalFeat(x)
        x = x * xa
        x = x.view(x.size(0), self.embed_feat, -1).sum(2)

        return x

class resnet_E1(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False,illum_inv = True):
        super(resnet_E1, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        R = preTmodels.resnext50_32x4d(pretrained=True)
        R = list(R.children())

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 2),
                                *convBlock1(in_ch = 32, out_ch = 64, ker = 3, stride = 1),
                                nn.MaxPool2d(3, 2, 1)) #40x40

        self.L2 = nn.Sequential(*R[4:6])


        self.ATTEN = nn.Sequential(nn.Conv2d(in_channels=512, out_channels=256, kernel_size=1, bias=False),
                                   nn.ReLU(True),
                                   nn.Conv2d(in_channels=256, out_channels=1, kernel_size=1, bias=False),
                                   nn.Softplus())

        self.embed_feat = 512

        del R

    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        x = self.L2(x)

        xa = self.ATTEN(x)

        return x, xa

    def forward(self, x):

        x, xa = self.getLocalFeat(x)
        x = x * xa
        x = x.view(x.size(0), self.embed_feat, -1).sum(2)

        return x




class MODEL7(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16, init = True):
        super(MODEL7, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 48, ker = 3),
                                nn.MaxPool2d(3, 2, 1),
                                densenet_block(in_ch = 48))#80x80
        self.L2 = nn.Sequential(nn.MaxPool2d(2, 2),
                                *convBlock1(in_ch = 48, out_ch = 96, ker = 1),
                                densenet_block(in_ch = 96),
                                densenet_block(in_ch = 96),
                                *convBlock1(in_ch = 96, out_ch = 128, ker = 1),
                                densenet_block(in_ch = 128),
                                densenet_block(in_ch = 128))#40x40


        self.clusters = nn.Parameter(torch.randn(16, 128))
        if init:
            self.clusters.data = torch.nn.init.xavier_uniform_(self.clusters.data)

        self.embed_feat = 128 * 16

        self.regions = None#genRegionMask(scale = 1, w = 10, s1 = 7, s2 = 3, s3 = 2, s4 = 2)


    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        x = self.L2(x)
        xa = F.softmax(F.conv2d(x, self.clusters.unsqueeze(-1).unsqueeze(-1)), dim=1)

        return x, xa


    def forward(self, x):
        x, xa = self.getLocalFeat(x)

        b, f, h, w = x.size()
        c = xa.size(1)

        x = x.view(b, f, -1).unsqueeze(1) - self.clusters.unsqueeze(0).unsqueeze(-1)
        xa = xa.view(b, c, -1).unsqueeze(2)

        x = x * xa

        x = x.sum(-1)
        x = F.normalize(x, dim=-1)
        x = x.view(b, -1)
        return x

class spatialSoftMax(nn.Module):
    def __init__(self):
        super(spatialSoftMax, self).__init__()

    def forward(self, x):
        b, c, h, w = x.size()
        return F.softmax(x.view(b, c, -1), dim=2).view(b, c, h, w)

class MODEL8(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16, init = True, scale = 1.0):
        super(MODEL8, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 48, ker = 3),
                                nn.MaxPool2d(3, 2, 1),
                                densenet_block(in_ch = 48))#80x80
        self.L2 = nn.Sequential(nn.MaxPool2d(2, 2),
                                *convBlock1(in_ch = 48, out_ch = 96, ker = 1),
                                densenet_block(in_ch = 96),
                                densenet_block(in_ch = 96),
                                *convBlock1(in_ch = 96, out_ch = 128, ker = 1),
                                densenet_block(in_ch = 128),
                                densenet_block(in_ch = 128))#40x40



        self.ATTEN = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=128, kernel_size=1, bias=False),
                                   nn.ReLU(True),
                                   nn.Conv2d(in_channels=128, out_channels=1, kernel_size=1, bias=False),
                                   nn.Sigmoid())


        self.regions = genRegionMask2(w = 40, r = [[0, 15], [5, 25], [15, 35], [25, 40]])#r = [[0, 20],[10, 30],[20, 40]])#
        self.regions.data.requires_grad = False
        self.embed_feat = 128*9


    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        x = self.L2(x)
        xa = self.ATTEN(x)

        return x, xa

    def forward(self, x):
        x, xa = self.getLocalFeat(x)

        b, f, h, w = x.size()

        x = x * xa

        x = x.view(b, f, h*w).unsqueeze(2) * self.regions
        x = F.normalize(x.sum(-1), dim = 1)
        x = x.view(b, -1)
        return x


class mobile_net_v2(nn.Module):
    def __init__(self, illum_inv = True, in_ch = 1, layers = "full", pool = 1, pretrained=True, adp_pool = True):
        super(mobile_net_v2, self).__init__()

        model_mods = {"full" : [19, 1280], "skip_1" : [18, 320], "skip_2" : [17, 160], "skip_3" : [14, 96], "skip_4" : [11, 64]}

        if illum_inv:
            self.sqi = SQI_mean()

        model = []
        in_layer_start = 0
        if illum_inv and in_ch == 1:
            model.append(SQI_mean())
            in_layer_start = 1
        if in_ch == 1:
            in_layer_start = 1

        R = preTmodels.mobilenet_v2(pretrained=pretrained)
        if in_layer_start == 1:
            model += convBlock(in_ch = 1, out_ch = 32, ker = 3, stride = 2)

        model +=  list(list(R.children())[0].children())[in_layer_start:model_mods[layers][0]]

        pool_layer = nn.AdaptiveAvgPool2d(pool) if adp_pool else nn.AvgPool2d(7, 3)

        self.feat = nn.Sequential(*model,
                                  nn.AdaptiveAvgPool2d(1),
                                  nn.Flatten()) #10x10 (4X stride = 2)


        self.embed_feat = model_mods[layers][1] * pool**2
        del R

    def forward(self, x):
        return self.feat(x)