# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-25T10:49:10-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-29T16:35:19-05:00
import numpy as np
import torch
import torch.nn as nn
from torch.autograd.function import Function
import torch.nn.functional as F
from torch.autograd import Variable

class LGMLoss(nn.Module):
    """
    Refer to paper:
    Weitao Wan, Yuanyi Zhong,Tianpeng Li, Jiansheng Chen
    Rethinking Feature Distribution for Loss Functions in Image Classification. CVPR 2018
    re-implement by yirong mao
    2018 07/02

    https://github.com/YirongMao/softmax_variants/blob/master/model_utils.py
    """
    def __init__(self, num_classes, feat_dim, alpha):
        super(LGMLoss, self).__init__()
        self.feat_dim = feat_dim
        self.num_classes = num_classes
        self.alpha = alpha

        self.centers = nn.Parameter(torch.randn(num_classes, feat_dim))
        self.log_covs = nn.Parameter(torch.zeros(num_classes, feat_dim))

    def forward(self, feat, label):
        batch_size = feat.shape[0]
        log_covs = torch.unsqueeze(self.log_covs, dim=0)


        covs = torch.exp(log_covs) # 1*c*d
        tcovs = covs.repeat(batch_size, 1, 1) # n*c*d
        diff = torch.unsqueeze(feat, dim=1) - torch.unsqueeze(self.centers, dim=0)
        wdiff = torch.div(diff, tcovs)
        diff = torch.mul(diff, wdiff)
        dist = torch.sum(diff, dim=-1) #eq.(18)


        y_onehot = torch.FloatTensor(batch_size, self.num_classes)
        y_onehot.zero_()
        y_onehot = Variable(y_onehot).cuda()
        y_onehot.scatter_(1, torch.unsqueeze(label, dim=-1), self.alpha)
        y_onehot = y_onehot + 1.0
        margin_dist = torch.mul(dist, y_onehot)

        slog_covs = torch.sum(log_covs, dim=-1) #1*c
        tslog_covs = slog_covs.repeat(batch_size, 1)
        margin_logits = -0.5*(tslog_covs + margin_dist) #eq.(17)
        logits = -0.5 * (tslog_covs + dist)

        cdiff = feat - torch.index_select(self.centers, dim=0, index=label.long())
        cdist = cdiff.pow(2).sum(1).sum(0) / 2.0

        slog_covs = torch.squeeze(slog_covs)
        reg = 0.5*torch.sum(torch.index_select(slog_covs, dim=0, index=label.long()))
        likelihood = (1.0/batch_size) * (cdist + reg)

        return logits, margin_logits, likelihood


class TotalLMGM(nn.Module):
    def __init__(self, num_classes, feat_dim, alpha, loss_weight = 0.1):
        super(TotalLMGM, self).__init__()
        self.lmgmloss = LGMLoss(num_classes, feat_dim, alpha)
        self.nllloss = nn.CrossEntropyLoss()

        self.loss_weight = loss_weight

    def forward(self, feat, target):

        logits, mlogits, likelihood = self.lmgmloss(feat, target)
        loss = self.nllloss(mlogits, target) + self.loss_weight * likelihood

        # _, predicted = torch.max(logits.data, 1)

        return loss, logits

class AdaCos(nn.Module):
    def __init__(self, in_feats, n_classes, scale = 14.0, m=0.50, use_s_cos = True):
        super(AdaCos, self).__init__()
        self.num_features = in_feats
        self.n_classes = n_classes
        self.s = scale#math.sqrt(2) * math.log(num_classes - 1)
        self.m = m
        self.W = nn.Parameter(torch.FloatTensor(n_classes, in_feats))
        nn.init.xavier_uniform_(self.W)
        self.cross_entropy = nn.CrossEntropyLoss()
        self.use_s_cos = use_s_cos

    def forward(self, input, label=None):
        # normalize features
        x = F.normalize(input)
        # normalize weights
        W = F.normalize(self.W)
        # dot product
        logits = F.linear(x, W)
        if label is None:
            return logits
        # feature re-scale
        theta = torch.acos(torch.clamp(logits, -1.0 + 1e-7, 1.0 - 1e-7))
        one_hot = torch.zeros_like(logits)
        one_hot.scatter_(1, label.view(-1, 1).long(), 1)
        with torch.no_grad():
            B_avg = torch.where(one_hot < 1, torch.exp(self.s * logits), torch.zeros_like(logits))
            B_avg = torch.sum(B_avg) / input.size(0)
            # print(B_avg)
            theta_med = torch.median(theta[one_hot == 1])
            self.s = torch.log(B_avg) / torch.cos(torch.min(np.pi/4 * torch.ones_like(theta_med), theta_med))
        output = self.s * logits
        output = self.cross_entropy(output, label)

        if self.use_s_cos:
            return output, logits
        else:
            return output

class SphereFace(nn.Module):
    def __init__(self, in_feats, n_classes, scale=30.0, m=4.0, use_s_cos = True):
        super(SphereFace, self).__init__()
        self.num_features = in_feats
        self.n_classes = n_classes
        self.s = scale
        self.m = m
        self.W = nn.Parameter(torch.FloatTensor(n_classes, in_feats))
        nn.init.xavier_uniform_(self.W)
        self.cross_entropy = nn.CrossEntropyLoss()

        self.use_s_cos = use_s_cos

    def forward(self, input, label=None):
        # normalize features
        x = F.normalize(input)
        # normalize weights
        W = F.normalize(self.W)
        # dot product
        logits = F.linear(x, W)

        if label is None:
            return logits
        # add margin
        theta = torch.acos(torch.clamp(logits, -1.0 + 1e-7, 1.0 - 1e-7))

        target_logits = torch.cos(self.m * theta)
        one_hot = torch.zeros_like(logits)
        one_hot.scatter_(1, label.view(-1, 1).long(), 1)
        output = logits * (1 - one_hot) + target_logits * one_hot
        # feature re-scale
        output *= self.s

        loss = self.cross_entropy(output, label)

        if self.use_s_cos:
            return loss, output
        else:
            return loss

class SphereLoss(nn.Module):
    def __init__(self, in_feats, n_classes, scale = 14, use_s_cos = True, *args, **kwargs):
        super(SphereLoss, self).__init__(*args, **kwargs)
        self.scale = scale
        self.cross_entropy = nn.CrossEntropyLoss()
        self.W = torch.nn.Parameter(torch.randn(in_feats, n_classes),
                requires_grad = True)
        #  nn.init.kaiming_normal_(self.W, a=1)
        nn.init.xavier_normal_(self.W, gain=1)

        self.use_s_cos = use_s_cos

    def forward(self, x, label):
        x_norm = torch.norm(x, 2, 1, True).clamp(min = 1e-12).expand_as(x)
        x_norm = x / x_norm
        w_norm = torch.norm(self.W, 2, 0, True).clamp(min = 1e-12).expand_as(self.W)
        w_norm = self.W / w_norm
        cos_th = torch.mm(x_norm, w_norm)
        s_cos_th = self.scale * cos_th
        loss = self.cross_entropy(s_cos_th, label)
        if self.use_s_cos:
            return loss, s_cos_th
        else:
            return loss


class SphereLoss_local(nn.Module):
    def __init__(self, in_feats, n_classes, scale = 14, use_s_cos = True, *args, **kwargs):
        super(SphereLoss_local, self).__init__(*args, **kwargs)

        x = np.ones((5, 5))
        np.fill_diagonal(x, val=0)
        self.neg_region = torch.nn.Parameter(torch.from_numpy(np.float32(np.expand_dims(np.expand_dims(x, 0), 0))),
                                             requires_grad = False)
        self.scale = scale
        self.cross_entropy = nn.CrossEntropyLoss()
        self.W = torch.nn.Parameter(torch.randn(in_feats, n_classes),
                requires_grad = True)
        #  nn.init.kaiming_normal_(self.W, a=1)
        nn.init.xavier_normal_(self.W, gain=1)

        self.use_s_cos = use_s_cos

    def forward(self, x, label, M, S):

        b = x.size(0)

        x_norm = torch.norm(x, 2, 1, True).clamp(min = 1e-12).expand_as(x)
        x_norm = x / x_norm
        w_norm = torch.norm(self.W, 2, 0, True).clamp(min = 1e-12).expand_as(self.W)
        w_norm = self.W / w_norm
        cos_th = torch.mm(x_norm, w_norm)
        s_cos_th = self.scale * cos_th
        loss = self.cross_entropy(s_cos_th, label)
        loss2 = F.nll_loss(M, S)

        if self.use_s_cos:
            return loss + loss2, s_cos_th, loss2
        else:
            return loss



import sklearn.preprocessing
def binarize_and_smooth_labels(T, nb_classes, smoothing_const = 0.1):

    T = T.cpu().numpy()
    T = sklearn.preprocessing.label_binarize(
        T, classes = range(0, nb_classes)
    )
    T = T * (1 - smoothing_const)
    T[T == 0] = smoothing_const / (nb_classes - 1)
    T = torch.FloatTensor(T).cuda()
    return T

class ProxyNCA(torch.nn.Module):
    def __init__(self, nb_classes, sz_embed, smoothing_const = 0.1, **kwargs):
        torch.nn.Module.__init__(self)
        self.proxies = torch.nn.Parameter(torch.randn(nb_classes, sz_embed) / 8)
        self.smoothing_const = smoothing_const

    def forward(self, X, T):

        P = self.proxies
        P = 3 * F.normalize(P, p = 2, dim = -1)
        X = 3 * F.normalize(X, p = 2, dim = -1)
        D = pairwise_distance(
            torch.cat(
                [X, P]
            ),
            squared = True
        )[:X.size()[0], X.size()[0]:]

        T = binarize_and_smooth_labels(
            T = T, nb_classes = len(P), smoothing_const = self.smoothing_const
        )

        # cross entropy with distances as logits, one hot labels
        # note that compared to proxy nca, positive not excluded in denominator
        logs = F.log_softmax(D, -1)
        loss = torch.sum(- T * logs, -1)

        return loss.mean(), logs


import sklearn


def pairwise_distance(a, squared=False):
    """Computes the pairwise distance matrix with numerical stability."""
    pairwise_distances_squared = torch.add(
        a.pow(2).sum(dim=1, keepdim=True).expand(a.size(0), -1),
        torch.t(a).pow(2).sum(dim=0, keepdim=True).expand(a.size(0), -1)
    ) - 2 * (
        torch.mm(a, torch.t(a))
    )

    # Deal with numerical inaccuracies. Set small negatives to zero.
    pairwise_distances_squared = torch.clamp(
        pairwise_distances_squared, min=0.0
    )

    # Get the mask where the zero distances are at.
    error_mask = torch.le(pairwise_distances_squared, 0.0)

    # Optionally take the sqrt.
    if squared:
        pairwise_distances = pairwise_distances_squared
    else:
        pairwise_distances = torch.sqrt(
            pairwise_distances_squared + error_mask.float() * 1e-16
        )

    # Undo conditionally adding 1e-16.
    pairwise_distances = torch.mul(
        pairwise_distances,
        (error_mask == False).float()
    )

    # Explicitly set diagonals to zero.
    mask_offdiagonals = 1 - torch.eye(
        *pairwise_distances.size(),
        device=pairwise_distances.device
    )
    pairwise_distances = torch.mul(pairwise_distances, mask_offdiagonals)

    return pairwise_distances
