# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:01:33-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-07-31T22:26:22-05:00
import os
import time
import numpy as np
import pyprind

import torch
from torch import optim, nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
cudnn.benchmark = True

from tensorboardX import SummaryWriter

from twrap.metrics import accuracy_topk
from twrap.utils import AverageMeter, tensor_to_torch_var, save_checkpoint

def train(model, train_data_loader, optimizer,
          criterion, train_batches,
          use_cuda, scheduler):

    totalloss_meter = AverageMeter()
    acc_meter = AverageMeter()

    # switch to train mode
    model.train()
    # progress bar init
    bar = pyprind.ProgPercent(train_batches, update_interval=1)
    tic = time.time()
    for batch, (I1, I2, T0) in enumerate(train_data_loader):


        I1 = tensor_to_torch_var(I1, use_cuda)
        I2 = tensor_to_torch_var(I2, use_cuda)
        T0 = tensor_to_torch_var(T0, use_cuda, volatile=True)

        # Compute gradient and do optimizer step
        optimizer.zero_grad()
        # Compute output
        P0 = model(I1, I2)
        # Calculate loss
        total_loss = criterion(P0, T0)

        total_loss.backward()

        optimizer.step()

        if scheduler:
            scheduler.step()

        toc = time.time() - tic
        tic = time.time()

        # Metrics
        totalloss_meter.update(total_loss.data.cpu().item(), T0.shape[0])
        acc = accuracy_topk(P0, T0, topk=(1,))[0]
        acc_meter.update(acc.data.cpu().item(), T0.shape[0])

        # Update log progress bar
        log_ = ' loss:'+ '{0:4.4f}'.format(totalloss_meter.avg)
        log_ += ' acc1:'+ '{0:4.4f}'.format(acc_meter.avg)
        log_ += ' batch time:'+ '{0:2.3f}'.format(toc)
        bar.update(item_id = log_)

        del I1, I2, T0, P0, acc

    return totalloss_meter.avg, acc_meter.avg


def fit_model(model, train_data_loader, train_batches, optimizer = None, criterion = None, batch_size = 64,
              num_epochs = 100, init_epoch = 1, scheduler_steps = None,
              log_dir = None, log_instance = None, use_cuda = True, resume_train = True):

    # Tensorboard Logger
    if log_dir is not None:
        if log_instance is not None:
            train_logger = SummaryWriter(log_dir+'/'+log_instance+'_train')

    # Best results save model
    best_loss = float('inf')
    is_best = False
    weights_loc = log_dir+'/weights/'+log_instance
    if not os.path.exists(weights_loc):
        os.makedirs(weights_loc)

    # Initializing cuda
    device = torch.device("cuda" if use_cuda else "cpu")
    if use_cuda:
        model.to(device)
        criterion = criterion.to(device)

    # Check and resume training
    check_point_file = weights_loc + '/checkpoint.pth.tar'
    if resume_train:
        if os.path.isfile(check_point_file):
            print("=> loading checkpoint '{}'".format(check_point_file))
            checkpoint = torch.load(check_point_file)
            init_epoch = checkpoint['epoch']+1
            best_loss = checkpoint['Loss']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(check_point_file, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(check_point_file))

    if scheduler_steps is not None:
        scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=scheduler_steps, gamma=0.1)

    for epoch in range(init_epoch, num_epochs+1):

        if scheduler_steps is not None:
            scheduler.step()

        totalloss, acc = train(model, train_data_loader, optimizer,
                                      criterion, train_batches,
                                      use_cuda, scheduler)

        train_logger.add_scalar('loss', totalloss, epoch)
        train_logger.add_scalar('acc', acc, epoch)
        if totalloss <= best_loss:
            is_best = True
            best_loss = totalloss
        else:
            is_best = False
        save_checkpoint({
            'epoch': epoch,
            'arch': log_instance,
            'state_dict': model.state_dict(),
            'Loss': best_loss,
            'optimizer' : optimizer.state_dict(),
        }, is_best, storage_loc = weights_loc)

    return model
