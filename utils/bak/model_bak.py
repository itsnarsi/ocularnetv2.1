# @Author: Narsi Reddy <narsi>
# @Date:   2019-07-31T21:55:24-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-31T21:55:25-05:00
class MODEL1(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False):
        super(MODEL1, self).__init__()

        self.L1 = nn.Sequential(*blockType1(in_ch = 1, out_ch = 64, ker = 3),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #64x64
                                *blockType1(in_ch = 64, out_ch = 64, ker = 3),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                *blockType1(in_ch = 64, out_ch = 128, ker = 3))
        self.L2 = nn.Sequential(nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                *blockType1(in_ch = 128, out_ch = 128, ker = 3))

        if classify:
            self.classify  = nn.Linear(in_features=256, out_features=nb_class)
        else:
            self.classify = None

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=128, kernel_size=1, bias=False),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=128, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())

            self.A2 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=128, kernel_size=1, bias=False),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=128, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False


        self.gp1 = nn.AdaptiveAvgPool2d(1) #Global Pooling
        self.gp2 = nn.AdaptiveAvgPool2d(1)

        self.embed_feat = 256

    def getLocalFeat(self, x):
        x1 = self.L1(x)
        x2 = self.L2(x1)
        if self.attention:
            x1a = self.A1(x1)
            x2a = self.A1(x2)

            x1 = nn.functional.normalize(x1)
            x2 = nn.functional.normalize(x2)

            return x1, x2, x1a, x2a
        return x1, x2

    def forward(self, x):
        x1 = self.L1(x)
        print(x1)
        x2 = self.L2(x1)

        if self.attention:
            x1a = self.A1(x1)
            x2a = self.A1(x2)

            x1 = x1 * x1a
            x1 = x1.view(x1.size(0), 128, -1)
            x1 = x1.sum(2)

            x2 = x2 * x2a
            x2 = x2.view(x2.size(0), 128, -1)
            x2 = x2.sum(2)

        else:
            x2 = self.gp1(x2)
            x1 = self.gp2(x1)

        x1 = L.flatten(x1)
        x2 = L.flatten(x2)

        x = torch.cat((x1, x2), dim = 1)

        x = nn.functional.normalize(x)# * 10.0

        if self.classify:
            x = self.classify(x)
        return x

class MODEL2(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False):
        super(MODEL2, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*blockType2(in_ch = 1, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #64x64
                                *blockType2(in_ch = 64, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                *blockType2(in_ch = 64, out_ch = 128, ker = 3, scale = 2)
                                )
        self.L2 = nn.Sequential(nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                *blockType2(in_ch = 128, out_ch = 128, ker = 3, scale = 2)
                                )

        if classify:
            self.classify  = nn.Linear(in_features=256, out_features=nb_class)
        else:
            self.classify = None

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=64, kernel_size=1, bias=False),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=64, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())

            self.A2 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=64, kernel_size=1, bias=False),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=64, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False



        self.gp1 = nn.AdaptiveAvgPool2d(4) #Global Pooling
        self.gp2 = nn.AdaptiveAvgPool2d(4)

        self.embed_feat = 256

    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x1 = self.L1(x)
        x2 = self.L2(x1)
        if self.attention:
            x1a = self.A1(x1)
            x2a = self.A1(x2)

            x1 = nn.functional.normalize(x1)
            x2 = nn.functional.normalize(x2)

            return x1, x2, x1a, x2a

        x1 = nn.functional.normalize(x1)
        x2 = nn.functional.normalize(x2)
        return x1, x2

    def getFeatures(self, x):

        if self.illInv:
            x = self.sqi(x)

        x1 = self.L1(x)
        x2 = self.L2(x1)
        # if self.attention:
        #     x1a = self.A1(x1)
        #     x2a = self.A1(x2)
        #
        #     x1 = x1 * x1a
        #     x2 = x2 * x2a
        x2 = self.gp1(x2)
        x1 = self.gp2(x1)

        x1 = L.flatten(x1)
        x2 = L.flatten(x2)

        x = torch.cat((x1, x2), dim = 1)

        return x


    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)

        x1 = self.L1(x)
        x2 = self.L2(x1)

        if self.attention:
            x1a = self.A1(x1)
            x2a = self.A1(x2)

            x1 = x1 * x1a
            x1 = x1.view(x1.size(0), 128, -1)
            x1 = x1.sum(2)

            x2 = x2 * x2a
            x2 = x2.view(x2.size(0), 128, -1)
            x2 = x2.sum(2)

        else:
            x2 = self.gp1(x2)
            x1 = self.gp2(x1)

        x1 = L.flatten(x1)
        x2 = L.flatten(x2)

        x = torch.cat((x1, x2), dim = 1)

        x = nn.functional.normalize(x)# * 10.0

        if self.classify:
            x = self.classify(x)
        return x

class MODEL2REG(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16):
        super(MODEL2REG, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*blockType2(in_ch = 1, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #64x64
                                *blockType2(in_ch = 64, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                *blockType2(in_ch = 64, out_ch = 128, ker = 3, scale = 2)
                                )
        self.L2 = nn.Sequential(nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                *blockType2(in_ch = 128, out_ch = 128, ker = 3, scale = 2)
                                )

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=128, kernel_size=1, bias=True),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=128, out_channels=1, kernel_size=1, bias=True),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False

        self.embed_feat = 128 * 5

        self.regions = genRegionMask()
        self.regions.data.requires_grad = False

    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        x = self.L2(x)

        if self.attention:
            x = x * self.A1(x)
        else:
            xa = None

        n, c, h, w = x.size()
        x = x.view(n, c, 1, h*w) * self.regions

        x = x.sum(-1)

        x = L.flatten(x)

        return x


class MODEL3(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16):
        super(MODEL3, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 24, ker = 5, stride = 2),
                                nn.ReLU(),
                                #64x64
                                L.RESNET_BLOCK(3, 24, filters = [96, 96, 96], use_bias=True,
                                               batch_norm = True, scale = 3.0, resnet_type = 2,
                                               pool_type = None),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                L.RESNET_BLOCK(3, 96, filters = [144, 144, 144, 144], use_bias=True,
                                               batch_norm = True, scale = 4.0, resnet_type = 2,
                                               pool_type = None),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                L.RESNET_BLOCK(3, 144, filters = [144, 144, 144, 144], use_bias=True,
                                               batch_norm = True, scale = 4.0, resnet_type = 2,
                                               pool_type = None),
                                )

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=144, out_channels=72, kernel_size=1, bias=True),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=72, out_channels=1, kernel_size=1, bias=True),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False

        self.embed_feat = 144

    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)


        x = self.L1(x)

        if self.attention:
            x = x * self.A1(x)
        x = x.view(x.size(0), 144, -1)
        x = x.sum(2)
        return x

        n, c, h, w = x1.size()
        x1 = x1.view(n, c, 1, h*w) * self.regions1
        x1 = x1.sum(-1)
        n, c, h, w = x2.size()
        x2 = x2.view(n, c, 1, h*w) * self.regions2
        x2 = x2.sum(-1)

        x = torch.cat((x1, x2), dim = 1)

        x = L.flatten(x)

        return L.flatten(x1)


class MODEL4(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16):
        super(MODEL4, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                *convBlock1(in_ch = 32, out_ch = 16, ker = 1),
                                #64x64
                                RBConv(in_ch = 16, out_ch = 16, expand = 4),
                                RBConv(in_ch = 16, out_ch = 24, expand = 4),
                                nn.ReLU6(True),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                RBConv(in_ch = 24, out_ch = 24, expand = 4),
                                RBConv(in_ch = 24, out_ch = 48, expand = 4),
                                nn.ReLU6(True),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                RBConv(in_ch = 48, out_ch = 96, expand = 4),
                                RBConv(in_ch = 96, out_ch = 96, expand = 4),
                                RBConv(in_ch = 96, out_ch = 96, expand = 4),
                                nn.BatchNorm2d(96),
                                nn.ReLU6(True),
                                nn.Conv2d(in_channels=96, out_channels=256, kernel_size=1, bias=False)
                                )

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=256, out_channels=256, kernel_size=1, bias=True),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=256, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False

        self.embed_feat = 256

        self.regions = None
        # self.regions.data.requires_grad = False

    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        if self.attention:
            xa = self.A1(x)
            x = nn.functional.normalize(x)
            return x, xa

        x1 = nn.functional.normalize(x1)
        x2 = nn.functional.normalize(x2)
        return x1, x2

    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)

        if self.attention:
            x = x * self.A1(x)

        x = x.view(x.size(0), 256, -1)
        x = x.sum(2)

        # n, c, h, w = x.size()
        # x = x.view(n, c, 1, h*w) * self.regions
        #
        # x = x.sum(-1)

        x = L.flatten(x)
        return x


class MODEL5(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False):
        super(MODEL5, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*blockType2(in_ch = 1, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #64x64
                                *blockType2(in_ch = 64, out_ch = 64, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #32x32
                                *blockType2(in_ch = 64, out_ch = 128, ker = 3, scale = 2),
                                nn.ReLU(),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #16x16
                                *blockType2(in_ch = 128, out_ch = 128, ker = 3, scale = 2)
                                )

        if classify:
            self.classify  = nn.Linear(in_features=128, out_features=nb_class)
        else:
            self.classify = None

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=128, out_channels=128, kernel_size=1, bias=False),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=128, out_channels=1, kernel_size=1, bias=False),
                                    nn.Softplus())
            self.attention = True
        else:
            self.attention = False



        self.gp = nn.AdaptiveAvgPool2d(4) #Global Pooling

        self.embed_feat = 128

        self.regions = genRegionMask()
        self.regions.data.requires_grad = False


    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)

        if self.attention:
            xa = self.A1(x)
        x = x * xa
        x = x.view(x.size(0), 128, -1)
        x = x.sum(2)
        x = L.flatten(x)
        if self.classify:
            x = self.classify(x)
        return x


class MODEL6(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16):
        super(MODEL6, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.L1 = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #80x80
                                *blockType2(in_ch = 32, out_ch = 32, ker = 3, scale = 2),
                                *blockType2(in_ch = 32, out_ch = 32, ker = 3, scale = 2),
                                nn.MaxPool2d(kernel_size=2, stride=2),
                                #40x40
                                *blockType2(in_ch = 32, out_ch = 128, ker = 3, scale = 4),
                                *blockType2(in_ch = 128, out_ch = 128, ker = 3, scale = 4),
                                *blockType2(in_ch = 128, out_ch = 256, ker = 3, scale = 4),
                                *blockType2(in_ch = 256, out_ch = 256, ker = 3, scale = 4),
                                )

        self.F1 = nn.Conv2d(in_channels=256, out_channels=768, kernel_size=1, bias=False)

        if attention:
            self.A1 = nn.Sequential(nn.Conv2d(in_channels=256, out_channels=256, kernel_size=1, bias=True),
                                    nn.ReLU(),
                                    nn.Conv2d(in_channels=256, out_channels=1, kernel_size=1, bias=False),
                                    nn.Sigmoid())
            self.attention = True
        else:
            self.attention = False

        self.embed_feat = 768

        self.regions = None
        # self.regions.data.requires_grad = False

    def getLocalFeat(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        if self.attention:
            xa = self.A1(x)
            x = nn.functional.normalize(x)
            return x, xa

        x1 = nn.functional.normalize(x1)
        x2 = nn.functional.normalize(x2)
        return x1, x2

    def forward(self, x):

        if self.illInv:
            x = self.sqi(x)

        x = self.L1(x)
        if self.attention:
            xa = self.A1(x)
        x = self.F1(x) * xa

        x = x.view(x.size(0), self.embed_feat, -1)
        x = x.sum(2)

        x = L.flatten(x)
        return x
