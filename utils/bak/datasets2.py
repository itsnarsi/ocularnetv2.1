# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:11:23-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-31T22:03:18-05:00
import os
from PIL import Image
import torch
torch.manual_seed(29)
from torch.utils.data import Dataset
import pandas as pd
import numpy as np
from glob import glob
np.random.seed(29)
from tqdm import tqdm
from .dataset_utils import VISOB_200_TRAIN
import itertools

def csv_cat(csv_files):
    img_pars = []
    for cf in csv_files:
        X = np.asarray(pd.read_csv(cf))
        img_pars.append(X)
    img_pars = np.concatenate(img_pars)
    return img_pars

class dataset_visob(Dataset):
    def __init__(self, csv_files, transform = None, visit = 1, set_200 = True, session1 = False):

        original_img_pars = csv_cat(csv_files)

        if set_200:
            img_pars = []
            for T in list(VISOB_200_TRAIN):
                img_pars.append(original_img_pars[original_img_pars[:, 1] == T, :])
            original_img_pars = np.concatenate(img_pars)

        if visit:
            img_pars = []
            for i in range(original_img_pars.shape[0]):
                if 'EC_VISIT_'+str(visit) in original_img_pars[i, 0]:
                    img_pars.append([original_img_pars[i, 0], original_img_pars[i, 1]])
            original_img_pars = np.asarray(img_pars, dtype = np.object)

        if session1:
            img_pars = []
            for i in range(original_img_pars.shape[0]):
                if '/S1/' in original_img_pars[i, 0]:
                    img_pars.append([original_img_pars[i, 0], original_img_pars[i, 1]])
            original_img_pars = np.asarray(img_pars, dtype = np.object)


        self.img_pars = original_img_pars
        targets = np.unique(self.img_pars[:, 1])
        self.num_targets = len(targets)

        self.targets = {}
        for i in range(self.num_targets):
            self.targets[targets[i]] = i

        self.num_samples = self.img_pars.shape[0]

        self.transform = transform

        print(self.img_pars.shape)
        print(self.num_targets)

    def __getitem__(self, ij):
        i, j = ij

        I1 = Image.open(self.img_pars[i, 0])
        I2 = Image.open(self.img_pars[j, 0])
        if self.transform:
            I1 = self.transform(I1)
            I2 = self.transform(I2)

        T1 = self.targets[self.img_pars[i, 1]]
        T2 = self.targets[self.img_pars[j, 1]]

        return I1, I2, int(1 * (T1 == T2))

    def __len__(self):
        return self.num_samples

class SiameseBatch(object):
    def __init__(self, targets, batch_size, nb_pos = 10, nb_neg = 10):

        self.targets = targets
        self.batch_size = batch_size
        self.unique_targets = np.unique(targets)

        self.nbp = nb_pos
        self.nbn = nb_neg

        pairs = self.genPairs()
        self.nb_pairs = len(pairs)

    def genPairs(self):
        pairs = []
        ids_ = np.arange(len(self.targets))
        for sid in tqdm(list(self.unique_targets)):
            p_locs = ids_[self.targets == sid]
            x = list(itertools.combinations(list(p_locs), 2))
            np.random.shuffle(x)
            nbp = self.nbp if len(x) >= self.nbp else len(x)
            pairs += x[:nbp]

            n_locs = ids_[self.targets != sid]
            np.random.shuffle(n_locs)
            np.random.shuffle(p_locs)

            nbn = self.nbn if len(p_locs) >= self.nbn else len(p_locs)

            for x in range(nbn):
                pairs.append((p_locs[x], n_locs[x]))

        print('Generated :' + str(len(pairs)) + ' siamese pairs')
        np.random.shuffle(pairs)
        return pairs

    def __iter__(self):
        pairs = self.genPairs()
        batch = []
        for i in range(len(pairs)):
            batch.append(list(pairs[i]))
            if len(batch) == self.batch_size:
                yield batch
                batch = []
        if len(batch) > 0:
            yield batch

    def __len__(self):
        return (self.nb_pairs + self.batch_size - 1) // self.batch_size
