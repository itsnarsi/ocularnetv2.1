# @Author: Narsi Reddy <narsi>
# @Date:   2019-06-29T21:10:20-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-08T14:07:46-05:00

import torch
import torch.nn as nn
import torch.nn.functional as F

class SQI_mean(nn.Module):
    def __init__(self, dropout = 0.0, old = False):
        super(SQI_mean, self).__init__()

        # self.layer = nn.Conv2d(1, 1, 33, bias=False)
        self.layer = nn.Conv2d(1, 1, 13, bias=False)
        self.layer.weight.data = (self.layer.weight.data * 0.0 + 1.0)/(13**2)
        self.layer.weight.requires_grad = False

        self.pad = nn.ReflectionPad2d(13//2)

    def forward(self, I):
        I = torch.clamp(I/(self.layer(self.pad(I)) + 1.5), 0, 1.5)/1.5
        return I

class densenet_block(nn.Module):
    def __init__(self, in_ch = 32):
        super(densenet_block, self).__init__()
        self.conv1 = nn.Sequential(*convBlock1(in_ch = in_ch, out_ch = in_ch//2, ker = 3))
        self.conv2 = nn.Sequential(*convBlock1(in_ch = in_ch//2, out_ch = in_ch//2, ker = 3))
    def forward(self, I):
        x1 = self.conv1(I)
        x2 = self.conv2(x1)
        return torch.cat((x1, x2), dim = 1)

class MaxConv(nn.Module):
    def __init__(self, in_ch = 32):
        super(MaxConv, self).__init__()
        self.maxP = nn.MaxPool2d(2, 2)
        self.avgP = nn.Sequential(nn.ReflectionPad2d(3//2),
                                  nn.AvgPool2d(3, 2, 0))

    def forward(self, I):
        x1 = self.maxP(I)
        x2 = self.avgP(I)
        return torch.cat((x1, x2), dim = 1)

def convBlock1(in_ch = 3, out_ch = 32, ker = 3, stride = 1, groups = 1):

    layers = []

    if ker > 1:
        layers += [nn.ReflectionPad2d(ker//2)]

    layers += [nn.Conv2d(in_channels=in_ch, out_channels=out_ch, kernel_size=ker, bias=True, stride = stride, groups = groups),
               nn.BatchNorm2d(out_ch),
               nn.ReLU(True),
               ]

    return layers

def blockType1(in_ch = 3, out_ch = 32, ker = 3):

    layers = [nn.ReflectionPad2d(ker//2),
              nn.Conv2d(in_channels=in_ch, out_channels=out_ch, kernel_size=ker, bias=True),
              nn.BatchNorm2d(out_ch),
              nn.ReLU(),
              nn.ReflectionPad2d(ker//2),
              nn.Conv2d(in_channels=out_ch, out_channels=out_ch, kernel_size=ker, bias=True),
              nn.BatchNorm2d(out_ch)
              ]

    return layers


def blockType2(in_ch = 3, out_ch = 32, ker = 3, scale = 2):

    layers = [nn.ReflectionPad2d(ker//2),
              nn.Conv2d(in_channels=in_ch, out_channels=out_ch//scale, kernel_size=ker, bias=True),
              nn.BatchNorm2d(out_ch//scale),
              nn.ReLU(),
              nn.ReflectionPad2d(ker//2),
              nn.Conv2d(in_channels=out_ch//scale, out_channels=out_ch, kernel_size=ker, bias=True),
              nn.BatchNorm2d(out_ch)
              ]

    return layers

def convBlock2(in_ch = 3, out_ch = 32, ker = 3):

    layers = [nn.BatchNorm2d(in_ch),
              nn.ReLU(True),]

    if ker > 1:
        layers += [nn.ReflectionPad2d(ker//2)]

    layers += [nn.Conv2d(in_channels=in_ch, out_channels=out_ch, kernel_size=ker, bias=False)]

    return layers

def convBlock3(in_ch = 3, out_ch = 32, ker = 3, expand = 6):

    layers = [nn.Conv2d(in_channels=in_ch, out_channels=int(out_ch * expand), kernel_size=1, bias=False),
              nn.BatchNorm2d(int(out_ch * expand)),
              nn.ReLU6(True),
              nn.ReflectionPad2d(ker//2),
              nn.Conv2d(in_channels=int(out_ch * expand), out_channels=int(out_ch * expand), kernel_size=ker, bias=False, groups=int(out_ch * expand)),
              nn.BatchNorm2d(int(out_ch * expand)),
              nn.ReLU6(True),
              nn.Conv2d(in_channels=int(out_ch * expand), out_channels=out_ch, kernel_size=1, bias=False),
              ]

    return layers


class RBConv(nn.Module):
    def __init__(self, in_ch, out_ch, expand = 4, stride = 1, ker = 3, SE = 0.5):
        super(RBConv, self).__init__()
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.expand = expand
        self.stride = stride

        self.feat = nn.Sequential(*convBlock3(in_ch = in_ch, out_ch = out_ch, ker = ker, expand = expand))

    def forward(self, I):
        x = self.feat(I)
        if self.in_ch == self.out_ch:
            x = I + x
        return x



class NetVLAD(nn.Module):
    """NetVLAD layer implementation"""

    def __init__(self, num_clusters=64, dim=128, alpha=100.0,
                 normalize_input=True):
        """
        Args:
            num_clusters : int
                The number of clusters
            dim : int
                Dimension of descriptors
            alpha : float
                Parameter of initialization. Larger value is harder assignment.
            normalize_input : bool
                If true, descriptor-wise L2 normalization is applied to input.
        """
        super(NetVLAD, self).__init__()
        self.num_clusters = num_clusters
        self.dim = dim
        self.alpha = alpha
        self.normalize_input = normalize_input
        self.conv = nn.Conv2d(dim, num_clusters, kernel_size=(1, 1), bias=True)
        self.centroids = nn.Parameter(torch.rand(num_clusters, dim))
        self._init_params()

    def _init_params(self):
        self.conv.weight = nn.Parameter(
            (2.0 * self.alpha * self.centroids).unsqueeze(-1).unsqueeze(-1)
        )
        self.conv.bias = nn.Parameter(
            - self.alpha * self.centroids.norm(dim=1)
        )

    def forward(self, x, atten = None):
        N, C, H, W = x.size()

        if self.normalize_input:
            x = F.normalize(x, p=2, dim=1)  # across descriptor dim

        # soft-assignment
        soft_assign = self.conv(x).view(N, self.num_clusters, -1)
        soft_assign = F.softmax(soft_assign, dim=1)

        if atten is not None:
            atten = atten.view(N, 1, -1)
            soft_assign = soft_assign * atten

        x_flatten = x.view(N, C, -1)

        # calculate residuals to each clusters
        residual = x_flatten.expand(self.num_clusters, -1, -1, -1).permute(1, 0, 2, 3) - \
            self.centroids.expand(x_flatten.size(-1), -1, -1).permute(1, 2, 0).unsqueeze(0)
        residual *= soft_assign.unsqueeze(2)
        vlad = residual.sum(dim=-1)

        vlad = F.normalize(vlad, p=2, dim=2)  # intra-normalization
        vlad = vlad.view(x.size(0), -1)  # flatten
        vlad = F.normalize(vlad, p=2, dim=1)  # L2 normalize

        return vlad

import numpy as np
def genRegionMask(scale = 1, w = 10, s1 = 7, s2 = 3, s3 = 2, s4 = 2):

    X = np.zeros((1, 1, 5, w, w), dtype = np.float32)
    X[0, 0, 0, :s1, :s1] = 1.0
    X[0, 0, 0, :s1, s2:] = 1.0
    X[0, 0, 0, s2:, :s1] = 1.0
    X[0, 0, 0, s2:, s2:] = 1.0
    X[0, 0, 0, s3:-s3,s4:-s4] = 1.0

    X = X.reshape((1, 1, 5, w**2))

    return nn.Parameter(torch.from_numpy(X))

class localMatch(nn.Module):
    def __init__(self, w = 3, cuda = True):
        super(localMatch, self).__init__()

        self.w = w
        c1 = np.zeros([w**2, 1, w, w], dtype = np.float32)
        c = 0
        for i in range(w):
            for j in range(w):
                c1[c, :, i, j] = 1
                c += 1
        c2 = np.zeros([1, 1, w, w], dtype = np.float32)
        c2[:, :, w//2, w//2] = 1

        self.c1 = nn.Parameter(torch.from_numpy(c1))
        self.c2 = nn.Parameter(torch.from_numpy(c2))

    def __call__(self, I1, I1a, I2, I2a):

        I1 = F.normalize(I1)
        I2 = F.normalize(I2)

        b, f, h, w = I1.size()

        new_h = h - self.w + 1
        new_w = w - self.w + 1

        I1 = I1.view(b*f, 1, h, w)
        I1 = nn.functional.conv2d(I1, self.c2)
        I1 = I1.view(b, f, 1, new_h, new_w)

        I2 = I2.view(b*f, 1, h, w)
        I2 = nn.functional.conv2d(I2, self.c1)
        I2 = I2.view(b, f, self.w**2, new_h, new_w)

        I1a = nn.functional.conv2d(I1a, self.c2)
        I2a = nn.functional.conv2d(I2a, self.c1)

        I1I2 = (I1 * I2).sum(1)
        I1aI2a = I1a * I2a

        return I1I2 * I1aI2a


def genRegionMask2(w = 40, r = [[0, 15], [5, 25], [15, 35], [25, 40]]):

    X = np.zeros((1, len(r) * len(r), w, w), dtype = np.float32)
    c = 0
    for i in range(len(r)):
        for j in range(len(r)):
            X[0, c, r[i][0]:r[i][1], r[j][0]:r[j][1]] = 1
            c += 1
    X = X.reshape((1, 1, len(r) * len(r), w**2))
    return nn.Parameter(torch.from_numpy(X))


class NetVLAD(nn.Module):
    """NetVLAD layer implementation"""

    def __init__(self, num_clusters=64, dim=128, alpha=100.0,
                 normalize_input=True, dummy = False):
        """
        Args:
            num_clusters : int
                The number of clusters
            dim : int
                Dimension of descriptors
            alpha : float
                Parameter of initialization. Larger value is harder assignment.
            normalize_input : bool
                If true, descriptor-wise L2 normalization is applied to input.
        """
        super(NetVLAD, self).__init__()
        self.num_clusters = num_clusters
        self.dim = dim
        self.alpha = alpha
        self.normalize_input = normalize_input
        self.conv = nn.Conv2d(dim, num_clusters, kernel_size=(1, 1), bias=True)
        self.centroids = nn.Parameter(torch.rand(num_clusters, dim))
        self._init_params()

        self.dummy = dummy

    def _init_params(self):
        self.conv.weight = nn.Parameter(
            (2.0 * self.alpha * self.centroids).unsqueeze(-1).unsqueeze(-1)
        )
        self.conv.bias = nn.Parameter(
            - self.alpha * self.centroids.norm(dim=1)
        )

    def forward(self, x):
        N, C = x.shape[:2]

        if self.normalize_input:
            x = F.normalize(x, p=2, dim=1)  # across descriptor dim

        # soft-assignment
        soft_assign = self.conv(x).view(N, self.num_clusters, -1)
        soft_assign = F.softmax(soft_assign, dim=1)
        #


        x_flatten = x.view(N, C, -1)

        # calculate residuals to each clusters
        residual = x_flatten.expand(self.num_clusters, -1, -1, -1).permute(1, 0, 2, 3) - \
            self.centroids.expand(x_flatten.size(-1), -1, -1).permute(1, 2, 0).unsqueeze(0)
        residual *= soft_assign.unsqueeze(2)
        vlad = residual.sum(dim=-1)

        vlad = torch.split(vlad, self.num_clusters, dim = 1)[0].contiguous()

        vlad = F.normalize(vlad, p=2, dim=2)  # intra-normalization
        vlad = vlad.view(x.size(0), -1)  # flatten
        # vlad = F.normalize(vlad, p=2, dim=1)  # L2 normalize

        return vlad
