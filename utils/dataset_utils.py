# @Author: Narsi Reddy <narsi>
# @Date:   2019-05-01T08:46:14-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-05-01T08:46:35-05:00
import numpy as np
VISOB_200_TRAIN = np.asarray(['1147l', '1147r', '1154l', '1154r', '1158l', '1158r', '1160l',
'1160r', '1177l', '1177r', '1186l', '1186r', '1192l', '1192r',
'1198l', '1198r', '1200l', '1200r', '2381l', '2381r', '2382l',
'2382r', '2383l', '2383r', '2384l', '2384r', '2391l', '2391r',
'2512l', '2512r', '2516l', '2516r', '2521l', '2521r', '2526l',
'2526r', '2528l', '2528r', '2611l', '2611r', '2613l', '2613r',
'2618l', '2618r', '2710l', '2710r', '2713l', '2713r', '2715l',
'2715r', '2717l', '2717r', '2718l', '2718r', '2811l', '2811r',
'2812l', '2812r', '2815l', '2815r', '2912l', '2912r', '2914l',
'2914r', '2921l', '2921r', '2924l', '2924r', '2926l', '2926r',
'2928l', '2928r', '2930l', '2930r', '2931l', '2931r', '2936l',
'2936r', '2938l', '2938r', '2945l', '2945r', '2949l', '2949r',
'2953l', '2953r', '2954l', '2954r', '2955l', '2955r', '2956l',
'2956r', '2959l', '2959r', '2960l', '2960r', '2964l', '2964r',
'2970l', '2970r', '3110l', '3110r', '3114l', '3114r', '3117l',
'3117r', '3119l', '3119r', '3120l', '3120r', '3122l', '3122r',
'3126l', '3126r', '3135l', '3135r', '3146l', '3146r', '3151l',
'3151r', '3152l', '3152r', '3202l', '3202r', '3205l', '3205r',
'3206l', '3206r', '3207l', '3207r', '3209l', '3209r', '3212l',
'3212r', '3216l', '3216r', '3230l', '3230r', '3235l', '3235r',
'3239l', '3239r', '3240l', '3240r', '3242l', '3242r', '3244l',
'3244r', '3245l', '3245r', '3246l', '3246r', '3247l', '3247r',
'3249l', '3249r', '3250l', '3250r', '3251l', '3251r', '3253l',
'3253r', '3255l', '3255r', '3256l', '3256r', '3257l', '3257r',
'3258l', '3258r', '3261l', '3261r', '3263l', '3263r', '3264l',
'3264r', '3267l', '3267r', '3268l', '3268r', '3273l', '3273r',
'3278l', '3278r', '3280l', '3280r', '3284l', '3284r', '3287l',
'3287r', '3288l', '3288r', '3289l', '3289r', '3293l', '3293r',
'3296l', '3296r', '3300l', '3300r', '3301l', '3301r', '3302l',
'3302r', '3303l', '3303r', '3306l', '3306r', '3307l', '3307r',
'3308l', '3308r', '3313l', '3313r', '3314l', '3314r', '3316l',
'3316r', '3317l', '3317r', '3319l', '3319r', '3320l', '3320r',
'3330l', '3330r', '3331l', '3331r', '3332l', '3332r', '3334l',
'3334r', '3335l', '3335r', '3336l', '3336r', '3339l', '3339r',
'3341l', '3341r', '3342l', '3342r', '3344l', '3344r', '3345l',
'3345r', '3346l', '3346r', '3353l', '3353r', '3360l', '3360r',
'3362l', '3362r', '3364l', '3364r', '3365l', '3365r', '3366l',
'3366r', '3371l', '3371r', '3372l', '3372r', '3373l', '3373r',
'3374l', '3374r', '3377l', '3377r', '3381l', '3381r', '3382l',
'3382r', '3385l', '3385r', '3386l', '3386r', '3387l', '3387r',
'3390l', '3390r', '3393l', '3393r', '3394l', '3394r', '3397l',
'3397r', '3401l', '3401r', '3404l', '3404r', '3405l', '3405r',
'3407l', '3407r', '3412l', '3412r', '3413l', '3413r', '3414l',
'3414r', '3416l', '3416r', '3417l', '3417r', '3431l', '3431r',
'3435l', '3435r', '3436l', '3436r', '3437l', '3437r', '3440l',
'3440r', '3445l', '3445r', '3447l', '3447r', '3448l', '3448r',
'3450l', '3450r', '3455l', '3455r', '3457l', '3457r', '3461l',
'3461r', '3465l', '3465r', '3467l', '3467r', '3468l', '3468r',
'3470l', '3470r', '3471l', '3471r', '3475l', '3475r', '3476l',
'3476r', '3477l', '3477r', '3479l', '3479r', '3494l', '3494r',
'3495l', '3495r', '3500l', '3500r', '3501l', '3501r', '3502l',
'3502r', '3554l', '3554r', '3555l', '3555r', '3558l', '3558r',
'3566l', '3566r', '3567l', '3567r', '3568l', '3568r', '3570l',
'3570r', '5408l', '5408r', '5412l', '5412r', '5416l', '5416r',
'5418l', '5418r', '5420l', '5420r', '5421l', '5421r', '5426l',
'5426r', '5428l', '5428r', '5436l', '5436r', '5550l', '5550r',
'5554l', '5554r', '5555l', '5555r', '6001l', '6001r', '6004l',
'6004r'])
