# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:11:23-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-11-08T16:02:45-06:00
import os
from PIL import Image
import torch
torch.manual_seed(29)
from torch.utils.data import Dataset
from torch.utils.data.sampler import Sampler
import pandas as pd
import numpy as np
from glob import glob
np.random.seed(29)
from .dataset_utils import VISOB_200_TRAIN
from tqdm import tqdm

def csv_cat(csv_files):
    img_pars = []
    for cf in csv_files:
        X = np.asarray(pd.read_csv(cf))
        img_pars.append(X)
    img_pars = np.concatenate(img_pars)
    return img_pars

def visob_clean(original_img_pars, visit, set_200, session1):
    if set_200:
        img_pars = []
        for T in list(VISOB_200_TRAIN):
            img_pars.append(original_img_pars[original_img_pars[:, 1] == T, :])
        original_img_pars = np.concatenate(img_pars)

    if visit:
        img_pars = []
        for i in range(original_img_pars.shape[0]):
            if 'EC_VISIT_'+str(visit) in original_img_pars[i, 0]:
                img_pars.append([original_img_pars[i, 0], original_img_pars[i, 1]])
        original_img_pars = np.asarray(img_pars, dtype = np.object)

    if session1:
        img_pars = []
        for i in range(original_img_pars.shape[0]):
            if '/S1/' in original_img_pars[i, 0]:
                img_pars.append([original_img_pars[i, 0], original_img_pars[i, 1]])
        original_img_pars = np.asarray(img_pars, dtype = np.object)
    return original_img_pars

class dataset_visob(Dataset):
    def __init__(self, csv_files, other_dataset = None, transform = None, visit = 1, set_200 = True, session1 = False):

        original_img_pars = csv_cat(csv_files)
        original_img_pars = visob_clean(original_img_pars, visit, set_200, session1)

        if other_dataset is not None:
            other_dataset_pars = csv_cat(other_dataset)
            print(original_img_pars.shape)
            print(other_dataset_pars.shape)
            original_img_pars = np.concatenate((original_img_pars, other_dataset_pars))


        self.img_pars = original_img_pars
        targets = np.unique(self.img_pars[:, 1])
        self.num_targets = len(targets)

        self.targets = {}
        for i in range(self.num_targets):
            self.targets[targets[i]] = i

        self.num_samples = self.img_pars.shape[0]

        self.transform = transform

        print(self.img_pars.shape)
        print(self.num_targets)



    def __getitem__(self, i):

        I = Image.open(self.img_pars[i, 0])
        if self.transform:
            I1 = self.transform(I)

        return I1, int(self.targets[self.img_pars[i, 1]])

    def __len__(self):
        return self.num_samples

class test_dataset_visob(Dataset):
    def __init__(self, img_list, transform = None, glob_feat = False):

        original_img_list = img_list

        V200 = list(VISOB_200_TRAIN)
        self.img_list = []
        for img in original_img_list:
            sub_id = img.split(os.sep)[-1].split('_')
            sub_id = sub_id[0] + sub_id[1]
            if sub_id in V200:
                pass
            else:
                self.img_list.append(img)
        self.num_samples = len(self.img_list)

        self.transform = transform

        self.gf = glob_feat

    def __getitem__(self, i):
        img = self.img_list[i]
        I = Image.open(img)
        if self.transform:
            I = self.transform(I)
        if self.gf:
            ih_, lbp_, hog_ = template_feat(np.asarray(I))

        sub_id = img.split(os.sep)[-1].split('_')
        sub_id = sub_id[0] + sub_id[1]
        if self.gf:
            return lbp_, hog_, sub_id
        else:
            return I, sub_id


    def __len__(self):
        return self.num_samples

class dataset_nonvisob(Dataset):
    def __init__(self, imgs_fldr, img_type = '.bmp', transform = None):

        if type(img_type) is str:
            self.imgs = glob(imgs_fldr + os.sep + img_type)
        else:
            imgs = []
            for it_ in img_type:
                imgs += glob(imgs_fldr + os.sep + it_)
            self.imgs = imgs

        self.num_samples = len(self.imgs)

        self.transform = transform

    def __getitem__(self, i):

        I = Image.open(self.imgs[i]).resize((160, 160), Image.BICUBIC)
        # print(I.size)
        if self.transform:
            I = self.transform(I)
        # print(I.size())
        target = self.imgs[i].split(os.sep)[-1].replace('Img_', '').split('_')[0]

        return I, target

    def __len__(self):
        return self.num_samples

class RandomSiameseSampler(Sampler):
    def __init__(self, img_pars, targets, num_iters = 1000, batch_size = 32):
        self.imgs_locs = {t:np.where(img_pars[:, 1] == t)[0] for t in targets}
        self.num_iters = num_iters
        self.targets = targets
        self.batch_size = batch_size

        self.num_samples = self.num_iters * self.batch_size

    def get_epoch_list(self):
        imgs_locs = self.imgs_locs.copy()
        targets = self.targets
        count_locs = dict()
        for t in targets:
            count_locs[t] = len(imgs_locs[t])

        targets_ = np.random.choice(targets, self.num_samples)
        siamese = np.random.choice([True, False], self.num_samples)

        ids_ = []
        for T, S in tqdm(zip(list(targets_), list(siamese)), total = self.num_samples):
            pl_ = np.random.choice(count_locs[T], count_locs[T], replace=False)
            id1 = int(imgs_locs[T][pl_[0]])
            if S:
                id2 = int(imgs_locs[T][pl_[1]])
            else:
                x_ = targets.copy()
                x_.remove(T)
                np.random.shuffle(x_)
                x_ = x_[0]
                nl_ = np.random.choice(count_locs[x_], count_locs[x_], replace=False)
                id2 = int(imgs_locs[x_][nl_[0]])

            ids_.append([id1, id2, S])
        return ids_

    def __iter__(self):

        return iter(self.get_epoch_list())

    def __len__(self):
        return self.num_samples


class dataset_visob_siamese(Dataset):
    def __init__(self, csv_files, other_dataset = None, transform = None, visit = 1, set_200 = True, session1 = False):

        original_img_pars = csv_cat(csv_files)
        original_img_pars = visob_clean(original_img_pars, visit, set_200, session1)

        if other_dataset is not None:
            other_dataset_pars = csv_cat(other_dataset)
            print(original_img_pars.shape)
            print(other_dataset_pars.shape)
            original_img_pars = np.concatenate((original_img_pars, other_dataset_pars))


        self.img_pars = original_img_pars
        targets = np.unique(self.img_pars[:, 1])
        self.num_targets = len(targets)

        self.targets = {}
        for i in range(self.num_targets):
            self.targets[targets[i]] = i

        self.num_samples = self.img_pars.shape[0]

        self.transform = transform

        print(self.img_pars.shape)
        print(self.num_targets)

    def __getitem__(self, ij):

        i, j, S = ij

        I1 = Image.open(self.img_pars[i, 0])
        if self.transform: I1 = self.transform(I1)
        T1 = int(self.targets[self.img_pars[i, 1]])

        I2 = Image.open(self.img_pars[j, 0])
        if self.transform: I2 = self.transform(I2)
        T2 = int(self.targets[self.img_pars[j, 1]])

        return I1, I2, T1, T2, int(S * 1.0)

    def __len__(self):
        return self.num_samples
