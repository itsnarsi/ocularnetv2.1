# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:01:33-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-01T17:15:40-05:00
import os
import time
import numpy as np
import pyprind

import torch
from torch import optim, nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
cudnn.benchmark = True

import torchvision.utils as vutils

from tensorboardX import SummaryWriter

from twrap.metrics import accuracy_topk
from twrap.utils import AverageMeter, tensor_to_torch_var, save_checkpoint

from PIL import Image

def train(model, train_data_loader, optimizer, optimzer_center,
          criterion, train_batches,
          use_cuda):

    totalloss_meter = AverageMeter()
    acc1_meter = AverageMeter()
    acc5_meter = AverageMeter()
    mask_meter = AverageMeter()

    # switch to train mode
    model.train()
    # progress bar init
    bar = pyprind.ProgPercent(train_batches, update_interval=1)
    tic = time.time()
    for batch, (batch_data, batch_target) in enumerate(train_data_loader):

        input_var = tensor_to_torch_var(batch_data, use_cuda)
        target_var = tensor_to_torch_var(batch_target, use_cuda, volatile=True)

        # Compute gradient and do optimizer step
        optimizer.zero_grad()
        optimzer_center.zero_grad()
        # Compute output
        features, mask = model(input_var)

        # Calculate loss
        total_loss, predictions = criterion(features, target_var)
        b, m, c, h, w = mask.size()
        mask_loss = torch.abs(mask.view(b, m, -1).mean(-1) - 0.3).sum(-1).mean()

        # total_loss = 1e-3 * mask_loss + total_loss

        total_loss.backward()

        optimizer.step()
        optimzer_center.step()

        toc = time.time() - tic
        tic = time.time()

        # Metrics
        totalloss_meter.update(total_loss.data.cpu().item(), batch_data.shape[0])

        acc1, acc5 = accuracy_topk(predictions, target_var, topk=(1,5))
        acc1_meter.update(acc1[0], batch_data.shape[0])
        acc5_meter.update(acc5[0], batch_data.shape[0])
        mask_meter.update(mask_loss.data.cpu().item(), batch_data.shape[0])

        # Update log progress bar
        log_ = ' loss:'+ '{0:4.4f}'.format(totalloss_meter.avg)
        log_ += ' acc1:'+ '{0:4.4f}'.format(acc1_meter.avg)
        log_ += ' acc5:'+ '{0:4.4f}'.format(acc5_meter.avg)
        log_ += ' mask:'+ '{0:4.4f}'.format(mask_meter.avg)
        log_ += ' batch time:'+ '{0:2.3f}'.format(toc)
        bar.update(item_id = log_)

        # print('===============================')
        # print(model.STN.bias.cpu().data.numpy())
        # print('===============================')

        if train_batches-2 == batch:
            mask_1 = mask.cpu().data[0, ...]
            mask_1 = nn.functional.interpolate(mask_1, scale_factor=6, mode='nearest')
            mask_1 = vutils.make_grid(mask_1, normalize=True, scale_each=True)


        del total_loss, input_var, target_var, acc1, acc5, mask

    return totalloss_meter.avg, acc1_meter.avg, acc5_meter.avg, mask_meter.avg, mask_1#


def fit_model(model, train_data_loader, train_batches, optimizer = None, criterion = None, batch_size = 64,
              num_epochs = 100, init_epoch = 1, scheduler_steps = None,
              log_dir = None, log_instance = None, use_cuda = True, resume_train = True, loss_lr = 0.01):

    # Tensorboard Logger
    if log_dir is not None:
        if log_instance is not None:
            train_logger = SummaryWriter(log_dir+'/'+log_instance+'_train')

    # Best results save model
    best_loss = float('inf')
    is_best = False
    weights_loc = log_dir+'/weights/'+log_instance
    if not os.path.exists(weights_loc):
        os.makedirs(weights_loc)

    # Initializing cuda
    device = torch.device("cuda" if use_cuda else "cpu")
    if use_cuda:
        model.to(device)
        criterion = criterion.to(device)

    optimzer_center = optim.SGD(criterion.parameters(), lr=loss_lr, momentum=0.9)

    # Check and resume training
    check_point_file = weights_loc + '/checkpoint.pth.tar'
    if resume_train:
        if os.path.isfile(check_point_file):
            print("=> loading checkpoint '{}'".format(check_point_file))
            checkpoint = torch.load(check_point_file)
            init_epoch = checkpoint['epoch']+1
            best_loss = checkpoint['Loss']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            optimzer_center.load_state_dict(checkpoint['optimizer_center'])
            criterion.load_state_dict(checkpoint['criterion'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(check_point_file, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(check_point_file))

    if scheduler_steps is not None:
        scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=scheduler_steps, gamma=0.1)
        sheduler_center = optim.lr_scheduler.MultiStepLR(optimzer_center, milestones=scheduler_steps, gamma=0.1)

    for epoch in range(init_epoch, num_epochs+1):

        totalloss, acc1, acc5, mask_loss, mask_1 = train(model, train_data_loader, optimizer, optimzer_center,
                                      criterion, train_batches,
                                      use_cuda)#

        if scheduler_steps is not None:
            scheduler.step()
            sheduler_center.step()

        train_logger.add_scalar('loss', totalloss, epoch)
        train_logger.add_scalar('acc1', acc1, epoch)
        train_logger.add_scalar('acc5', acc5, epoch)
        train_logger.add_scalar('mask_loss', mask_loss, epoch)
        train_logger.add_image('masks', mask_1, epoch)

        train_logger.add_histogram('stn_bias', model.STN.bias.cpu().data.numpy(), epoch)
        train_logger.add_histogram('stn_weights', model.STN.weight.cpu().data.numpy(), epoch)

        if totalloss <= best_loss:
            is_best = True
            best_loss = totalloss
        else:
            is_best = False
        save_checkpoint({
            'epoch': epoch,
            'arch': log_instance,
            'state_dict': model.state_dict(),
            'Loss': best_loss,
            'optimizer' : optimizer.state_dict(),
            'optimizer_center' : optimzer_center.state_dict(),
            'criterion' : criterion.state_dict(),
        }, is_best, storage_loc = weights_loc)

    return model
