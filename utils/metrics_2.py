# @Author: Narsi Reddy <cibitaw1>
# @Date:   2019-10-01T16:09:14-05:00
# @Email:  sainarsireddy@outlook.com
# @Last modified by:   cibitaw1
# @Last modified time: 2019-10-01T17:39:15-05:00
import numpy as np
import torch
import torch.nn as nn
from torch.autograd.function import Function
import torch.nn.functional as F
from torch.autograd import Variable
from .metrics import SphereLoss


class MultiSphereLoss(nn.Module):
    def __init__(self, in_feats, n_classes, scale = 14, use_s_cos = True, nb_regions = 4, *args, **kwargs):
        super(MultiSphereLoss, self).__init__(*args, **kwargs)

        self.loss = nn.ModuleList([SphereLoss(in_feats, n_classes, scale) for i in range(nb_regions)])
        self.nbr = nb_regions

    def forward(self, x, label):
        loss = 0
        scores = []
        x_split = torch.split(x, 1, dim = 1)
        for i, L in enumerate(self.loss):
            l, s = L(x_split[i].squeeze(), label)
            loss += l
            scores.append(s.unsqueeze(1))
        scores = torch.cat(scores, 1).mean(1)

        return loss, scores
