# @Author: Narsi Reddy <narsi>
# @Date:   2019-08-24T16:21:27-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-11-08T16:58:47-06:00
import numpy as np
import torch
torch.manual_seed(29)
from torch import nn
from twrap import layers as L
import torch.nn.functional as F
from torchvision import models as preTmodels
from efficientnet_pytorch import EfficientNet
from .model_utils import *

def affine_weights3(w = 1.0, l = [0.0]):
    nb_w = len(l) ** 2
    X = torch.zeros(nb_w, 3, 3)
    X[:, 0, 0] = w
    X[:, 1, 1] = w
    X[:, 2, 2] = 1.0
    c = 0
    for i in l:
        for j in l:
            X[c, 0, 2] = i
            X[c, 1, 2] = j
            c += 1

    X = X[:, :2, :].contiguous()
    return nb_w, X.view(-1)

def affine_weights2(w = 0.5, l = [-0.5, 0.0, 0.5]):
    nb_w = len(l) ** 2
    X = torch.zeros(nb_w, 3, 3)
    X[:, 0, 0] = w
    X[:, 1, 1] = w
    X[:, 2, 2] = 1.0
    c = 0
    for i in l:
        for j in l:
            X[c, 0, 2] = i
            X[c, 1, 2] = j
            c += 1

    X = torch.inverse(X)[:, :2, :].contiguous()
    return nb_w, X.view(-1)

def affine_weights(nb_w):
    X = torch.zeros(nb_w, 2, 3)
    X[:, 0, 0] = 1/0.6
    X[:, 1, 1] = 1/0.6
    # Top-Left crop
    X[0, 0, 2] = 1/0.6-1
    X[0, 1, 2] = 1/0.6-1
    # Top-Right crop
    X[1, 0, 2] = 1-1/0.6
    X[1, 1, 2] = 1/0.6-1
    # Bottom-Left crop
    X[2, 0, 2] = 1/0.6-1
    X[2, 1, 2] = 1-1/0.6
    # Bottom-Right crop
    X[3, 0, 2] = 1-1/0.6
    X[3, 1, 2] = 1-1/0.6
    # # Center crops
    if nb_w == 5:
        X[4, 0, 2] = 0.0
        X[4, 1, 2] = 0.0
    return X.view(-1)

class STNMODEL_1(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False, num_clusters = 16, init = True, scale = 1.0, nb_w = 4):
        super(STNMODEL_1, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()
        self.nb_w = nb_w

        #
        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 24, ker = 3, stride = 1),
                                *convBlock1(in_ch = 24, out_ch = 48, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #80x80
                                *convBlock1(in_ch = 48, out_ch = 48, ker = 3, stride = 1),
                                *convBlock1(in_ch = 48, out_ch = 48, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #40x40
                                *convBlock1(in_ch = 48, out_ch = 96, ker = 3, stride = 1),
                                *convBlock1(in_ch = 96, out_ch = 96, ker = 3, stride = 1),
                                )

        self.LW = nn.Sequential(*convBlock1(in_ch = 96, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 128, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 128, out_features = 6*self.nb_w, bias=True)
        self.STN.bias.data = affine_weights(nb_w)
        # self.STN.weight.data = self.STN.weight.data * 0.0

        self.embed_feat = 96*self.nb_w


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def forward(self, x):
        x = self.getLocalFeat(x)
        b, f, h, w = x.size()

        affine = self.STN(self.LW(x)).view(b, -1, 2, 3).view(b * self.nb_w, 2, 3)
        mask = torch.ones(b*self.nb_w, 1, h, w)
        if x.is_cuda:
            mask = mask.cuda(x.get_device())
        mask = mask.type_as(x)

        grid = F.affine_grid(affine, mask.size())
        mask = F.grid_sample(mask, grid, mode='bilinear').view(b, self.nb_w, 1, h, w)



        xw = x.unsqueeze(1) * mask

        # x = torch.cat((xw, x.unsqueeze(1)), dim = 1)

        x = xw.view(b, self.nb_w, f, -1)
        x = F.normalize(x.sum(-1), dim = 2)
        x = x.view(b, -1)
        return x, mask

class STNMODEL_2(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False,
                 w = 1.0, l = [0.0]):
        #w = 0.5, l = [-0.5, 0.0, 0.5]
        super(STNMODEL_2, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 24, ker = 3, stride = 1),
                                *convBlock1(in_ch = 24, out_ch = 48, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #80x80
                                *convBlock1(in_ch = 48, out_ch = 48, ker = 3, stride = 1),
                                *convBlock1(in_ch = 48, out_ch = 96, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #40x40
                                *convBlock1(in_ch = 96, out_ch = 96, ker = 3, stride = 1),
                                *convBlock1(in_ch = 96, out_ch = 96, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #20x20
                                *convBlock1(in_ch = 96, out_ch = 128, ker = 3, stride = 1),
                                nn.Conv2d(in_channels = 128, out_channels = 128, kernel_size=3, stride = 1),
                                nn.AdaptiveAvgPool2d(2)
                                )

        nb_w, A = affine_weights3(w = w, l = l)
        self.nb_w = nb_w

        self.LW = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 128, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 128, out_features = 6, bias=True)

        self.STN.bias.data = A
        self.STN.weight.data = self.STN.weight.data * 0.0

        self.embed_feat = 128*4

        x = torch.ones(1, 2, 3)
        x[0, 0, 1] = 0.0
        x[0, 1, 0] = 0.0
        self.mask = nn.Parameter(x, requires_grad=False)


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def getIntrestRegion(self, I):
        x = F.interpolate(I, scale_factor=1/4, mode='bilinear', align_corners=True)
        aff = self.LW(x)
        aff = self.STN(aff).view(-1, 2, 3)# * self.mask

        grid = F.affine_grid(aff, I.size())
        Inew = F.grid_sample(I, grid, mode='bilinear')

        return Inew

    def forward(self, x):
        Inew = self.getIntrestRegion(x)
        feat = self.getLocalFeat(Inew)
        b, c, h, w = feat.size()
        feat = feat.view(b, c, -1)
        feat = F.normalize(feat, p=2, dim=1).view(b, -1)
        return feat, Inew

class STNMODEL_3(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False,
                 w = 1.0, l = [0.0]):
        #w = 0.5, l = [-0.5, 0.0, 0.5]
        super(STNMODEL_3, self).__init__()

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 24, ker = 3, stride = 1),
                                *convBlock1(in_ch = 24, out_ch = 48, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #80x80
                                *convBlock1(in_ch = 48, out_ch = 48, ker = 3, stride = 1),
                                *convBlock1(in_ch = 48, out_ch = 96, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #40x40
                                *convBlock1(in_ch = 96, out_ch = 96, ker = 3, stride = 1),
                                *convBlock1(in_ch = 96, out_ch = 96, ker = 3, stride = 1),
                                nn.MaxPool2d(2, 2),
                                #20x20
                                *convBlock1(in_ch = 96, out_ch = 128, ker = 3, stride = 1),
                                nn.ReflectionPad2d(1),
                                nn.Conv2d(in_channels = 128, out_channels = 128, kernel_size=3, stride = 1),
                                )

        self.pool = nn.AdaptiveAvgPool2d(2)

        nb_w, A = affine_weights3(w = w, l = l)
        self.nb_w = nb_w

        self.LW = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 128, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 128, out_features = 6, bias=True)

        self.STN.bias.data = A
        self.STN.weight.data = self.STN.weight.data * 0.0

        self.embed_feat = 128*4


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def getIntrestRegion(self, I):
        x = F.interpolate(I, scale_factor=1/4, mode='bilinear', align_corners=True)
        aff = self.LW(x)
        aff = self.STN(aff).view(-1, 2, 3)# * self.mask

        b = I.size(0)

        mask = torch.ones(b, 1, 20, 20)
        if x.is_cuda:
            mask = mask.cuda(x.get_device())
        mask = mask.type_as(x)

        grid = F.affine_grid(aff, I.size())
        Inew = F.grid_sample(I, grid, mode='bilinear')
        Mgrid = F.affine_grid(aff, mask.size())
        Mnew = F.grid_sample(mask, Mgrid, mode='nearest')

        return Inew, Mnew

    def forward(self, x):
        Inew, Mnew = self.getIntrestRegion(x)
        feat = self.getLocalFeat(Inew)
        b, c, h, w = feat.size()
        feat = feat * Mnew
        feat = self.pool(feat)
        feat = feat.view(b, c, -1)
        feat = F.normalize(feat, p=2, dim=1).view(b, -1)
        return feat, Inew

class STNMODEL_4(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False,
                 w = 1.0, l = [0.0]):
        #w = 0.5, l = [-0.5, 0.0, 0.5]
        super(STNMODEL_4, self).__init__()

        # mobilenet_v2 1 to 14 layers

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        R = preTmodels.mobilenet_v2(pretrained=True)
        R = list(list(R.children())[0].children())[1:14]

        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 1),
                                *R,
                                nn.AdaptiveAvgPool2d(2))

        nb_w, A = affine_weights3(w = w, l = l)
        self.nb_w = nb_w

        self.LW = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 128, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 128, out_features = 6, bias=True)

        self.STN.bias.data = A
        self.STN.weight.data = self.STN.weight.data * 0.0

        self.embed_feat = 96*4


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def getIntrestRegion(self, I):
        x = F.interpolate(I, scale_factor=1/4, mode='bilinear', align_corners=True)
        aff = self.LW(x)
        aff = self.STN(aff).view(-1, 2, 3)# * self.mask

        grid = F.affine_grid(aff, I.size())
        Inew = F.grid_sample(I, grid, mode='bilinear')

        return Inew

    def forward(self, x):
        Inew = self.getIntrestRegion(x)
        feat = self.getLocalFeat(Inew)
        b, c, h, w = feat.size()
        feat = feat.view(b, c, -1)
        feat = F.normalize(feat, p=2, dim=1).view(b, -1)
        return feat, Inew

class STNMODEL_5(nn.Module):
    def __init__(self, nb_class = 1001, classify = True, attention = False, illum_inv = False,
                 w = 1.0, l = [0.0]):
        #w = 0.5, l = [-0.5, 0.0, 0.5]
        super(STNMODEL_5, self).__init__()

        # ResNext50

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        R = EfficientNet.from_pretrained('efficientnet-b0')
        R = list(list(R.children())[2].children())[:11]

        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 1),
                                *R,
                                nn.AdaptiveAvgPool2d(2))

        nb_w, A = affine_weights3(w = w, l = l)
        self.nb_w = nb_w

        self.LW = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 128, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 128, out_features = 6, bias=True)

        self.STN.bias.data = A
        self.STN.weight.data = self.STN.weight.data * 0.0

        self.embed_feat = 112*4


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def getIntrestRegion(self, I):
        x = F.interpolate(I, scale_factor=1/4, mode='bilinear', align_corners=True)
        aff = self.LW(x)
        aff = self.STN(aff).view(-1, 2, 3)# * self.mask

        grid = F.affine_grid(aff, I.size())
        Inew = F.grid_sample(I, grid, mode='bilinear')

        return Inew

    def forward(self, x):
        Inew = self.getIntrestRegion(x)
        feat = self.getLocalFeat(Inew)
        b, c, h, w = feat.size()
        feat = feat.view(b, c, -1)
        feat = F.normalize(feat, p=2, dim=1).view(b, -1)
        return feat, Inew

class STNMODEL_4x(nn.Module):
    def __init__(self, illum_inv = False):
        super(STNMODEL_4x, self).__init__()

        # mobilenet_v2 1 to 14 layers

        self.illInv = illum_inv
        if illum_inv:
            self.sqi = SQI_mean()

        R = preTmodels.mobilenet_v2(pretrained=True)
        R = list(list(R.children())[0].children())[1:14]

        self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 1),
                                *R,
                                nn.AdaptiveAvgPool2d(1))

        nb_w, A = affine_weights3(w = 0.5, l = [-0.5, 0.5])
        self.nb_w = nb_w

        self.LW = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 16, ker = 3, stride = 2),
                                #20x20
                                nn.MaxPool2d(2, 2),
                                #10x10
                                *convBlock1(in_ch = 16, out_ch = 32, ker = 3, stride = 2),
                                #5x5
                                L.Flatten(),
                                nn.Linear(in_features = 25*32, out_features = 256, bias=True),
                                nn.ReLU(True))
        self.STN = nn.Linear(in_features = 256, out_features = 6*4, bias=True)
        # self.STN.bias.data = A
        # self.STN.weight.data = self.STN.weight.data# * 0.0

        self.embed_feat = 96*4


    def getLocalFeat(self, x):
        if self.illInv:
            x = self.sqi(x)
        x = self.LF(x)
        return x

    def getIntrestRegions(self, I):
        b, c, h_, w_ = I.size()
        x = F.interpolate(I, scale_factor=1/4, mode='bilinear', align_corners=True)
        aff = self.LW(x)
        aff = self.STN(aff).view(b, -1, 2, 3).split(1, dim = 1)

        Inew = []
        for aff_ in aff:
            grid = F.affine_grid(aff_.squeeze(1), (b, c, 64, 64))
            Inew.append(F.grid_sample(I, grid, mode='bilinear'))

        return Inew

    def forward(self, x):
        Inew = self.getIntrestRegions(x)
        feat = [self.getLocalFeat(Inew_) for Inew_ in Inew]
        feat = torch.cat(feat, dim = 1)
        b, c, h, w = feat.size()
        feat = F.normalize(feat, p=2, dim=1).view(b, -1)
        return feat, torch.cat(Inew, dim = 2)
#
# from matplotlib import pyplot as plt
# from PIL import Image
#
# I = Image.open('/media/cibitaw1/datasets/ubipr/imgs/C1_S1_L_D1P1.bmp')
#
# I1 = torch.from_numpy(np.float32(I.convert('L'))/255.0).unsqueeze(0).unsqueeze(0)
#
# x = torch.ones(400).view(4, 1, 10, 10)
# _, theta = affine_weights3(w = 0.5, l = [.0])
#
# grid = F.affine_grid(theta.view(-1, 2, 3), (1,1,72,72))#I1.size()
# x_trans = F.grid_sample(I1, grid, mode='nearest')
#
# plt.imshow(I1.squeeze().numpy())
# plt.imshow(x_trans.squeeze().numpy())
# plt.imshow(x_trans.squeeze().numpy())
#
#
# R = preTmodels.mobilenet_v2(pretrained=True)
# R = list(list(R.children())[0].children())#[1:14]
#
# self.LF = nn.Sequential(*convBlock1(in_ch = 1, out_ch = 32, ker = 3, stride = 1),
#                         *R)
#
# X = torch.randn(1, 3, 64, 64)
#
# R1 = nn.Sequential(*R[:14])
#
# R1(X).size()
#
# 96*4
#
# X.split(1, dim = 1)[0].size()
