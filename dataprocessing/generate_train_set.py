# @Author: Narsi Reddy <narsi>
# @Date:   2019-07-10T17:05:53-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-14T17:20:58-05:00
import os
from glob import glob
from PIL import Image
import numpy as np
from tqdm import tqdm
import pandas as pd

src_fldr = '/media/cibitaw1/DATA/MITEyeGaze'
dst_fldr = '/media/cibitaw1/datasets/OcularNetV2/MITGaze'

subject_set = np.load('./MIT_Gaze_subIDS.npy')

# img_pars  = [['imgs', 'subject']]
# for sub in tqdm(subject_set):
#     imgs = glob(src_fldr + os.sep + 'eye_crops_*' + os.sep + sub+'*.png')
#     for img in tqdm(imgs):
#         I = Image.open(img)
#         if I.size[0] >= 75:
#             img_name = img.split(os.sep)[-1]
#             sub_id = img_name.split('_')[0]
#             new_img_url = img.replace(src_fldr, dst_fldr + os.sep + 'imgs')
#             if not os.path.exists(os.path.split(new_img_url)[0]):
#                 os.makedirs(os.path.split(new_img_url)[0])
#
#             I.resize((96, 96), Image.BICUBIC).save(new_img_url)
#             img_pars.append([new_img_url, sub_id])
# pd.DataFrame(img_pars).to_csv(dst_fldr + os.sep + 'img_pars.csv', header=False, index=False)

dst_fldr = '/media/cibitaw1/datasets/OcularNetV2/MITGaze'
imgs = glob(dst_fldr + os.sep + 'imgs' + os.sep + '*' + os.sep + '*.png')

img_pars  = [['imgs', 'subject']]
c = 0
for img in tqdm(imgs):
    try:
        I = Image.open(img)
        I.verify()
        img_name = img.split(os.sep)[-1]
        sub_id = img_name.split('_')[0]
        img_pars.append([img, sub_id])
    except:
        print(img)
        os.remove(img)
pd.DataFrame(img_pars).to_csv(dst_fldr + os.sep + 'img_pars.csv', header=False, index=False)

c
