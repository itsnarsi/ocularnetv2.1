# @Author: Narsi Reddy <narsi>
# @Date:   2019-07-06T15:44:17-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-09-18T14:45:29-05:00
import os
from glob import glob
import json
from PIL import Image
import shutil
from tqdm import tqdm
import numpy as np
import cv2
import dlib

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

def genBatches(files, nb_batch = 8):
    batches = []
    batch = []
    for i in range(len(files)):
        batch.append(files[i])
        if len(batch) == nb_batch:
            batches.append(batch)
            batch = []
    if len(batch) > 0:
        batches.append(batch)
    return batches

class dlibFaceRed(object):

    def __init__(self, detector, landmarks):

        self.detector = detector
        self.landmarks =  landmarks

    def __call__(self, img1, img2, scale):
        point_cloud = []
        dets = self.detector(img1, 0)

        if len(dets) == 0:
            return (False,[],[])
        rects = dlib.rectangles()
        rects.extend([dlib.rectangle(left = int(d.rect.left()*scale),
                                     right = int(d.rect.right()*scale),
                                     top = int(d.rect.top()*scale),
                                     bottom = int(d.rect.bottom()*scale)) for d in dets])
        shape = self.landmarks(img2, rects[0])

        point_cloud = []
        for i in range(0, 5):
            point_cloud.append([shape.part(i).y, shape.part(i).x, ])
        point_cloud = np.asarray(point_cloud)

        bbox_re_ex = getBBOXExtended(point_cloud, a = [2, 3])
        bbox_le_ex = getBBOXExtended(point_cloud, a = [1, 0])


        return (True, bbox_re_ex, bbox_le_ex)

def getBBOXExtended(point_cloud, a = [1, 0]):
    x_min = point_cloud[a[0], 0]
    y_min = point_cloud[a[0], 1]
    x_max = point_cloud[a[1], 0]
    y_max = point_cloud[a[1], 1]
    cx = 0.5 * (x_min + x_max)
    cy = 0.5 * (y_min + y_max)
    W = (y_max - y_min)/0.60
    H = W
    bbox = [int(cx - H * 0.5), int(cx + H * 0.5), int(cy - W * 0.5), int(cy + W * 0.5)]

    return bbox

def detectAndExtractEYE(img, dlib_gen, dst_fldr):
    # try:
    sub_id = img.split(os.sep)[-3]
    frame_id = img.split(os.sep)[-1].replace('.jpg', '')
    I = Image.open(img).convert('RGB')

    scale_1 = I.size[1]/120
    scale_2 = I.size[1]/480
    I1 = I.resize((int(I.size[0]//scale_1), int(I.size[1]//scale_1)), Image.BICUBIC)
    I2 = I.resize((int(I.size[0]//scale_2), int(I.size[1]//scale_2)), Image.BICUBIC)

    detect, bbox_re_ex, bbox_le_ex = dlib_gen(np.uint8(I1.convert('L')).copy(), np.uint8(I2.convert('L')).copy(), scale_1/scale_2)

    if detect is True:

        bbox_re_ex = [int(i * scale_2) for i in bbox_re_ex]
        bbox_le_ex = [int(i * scale_2) for i in bbox_le_ex]

        right_eye = sub_id + 'R_' + frame_id + '.png'
        left_eye = sub_id + 'L_' + frame_id + '.png'

        I.crop([bbox_re_ex[2], bbox_re_ex[0], bbox_re_ex[3], bbox_re_ex[1]]).transpose(Image.FLIP_LEFT_RIGHT).save(os.path.join(dst_fldr, right_eye))
        I.crop([bbox_le_ex[2], bbox_le_ex[0], bbox_le_ex[3], bbox_le_ex[1]]).save(os.path.join(dst_fldr, left_eye))
    # except:
    #     pass

class genCrops(object):
    def __init__(self, dlib_gen, dst_fldr):
        self.dlib_gen = dlib_gen
        self.dst_fldr = dst_fldr

    def __call__(self, gzf):
        os.system('tar -xzf ' + gzf +' -C ' + src_fldr)
        gzff = gzf.replace('.tar.gz', '')
        frame_info = json.load(open( gzff + os.sep + 'frames.json'))
        for f in tqdm(frame_info):
            detectAndExtractEYE(gzff + os.sep + 'frames' + os.sep + f, self.dlib_gen, self.dst_fldr)
        shutil.rmtree(gzff)
        return 0


if __name__ == '__main__':
    src_fldr = '/media/cibitaw1/DATA/MITEyeGaze/extracted'
    dst_fldr = '/media/cibitaw1/DATA/MITEyeGaze/eye_crops'

    detector = dlib.cnn_face_detection_model_v1("/home/cibitaw1/local/ocularnetv2.1/dataprocessing/mmod_human_face_detector.dat")
    landmarks =  dlib.shape_predictor("/home/cibitaw1/local/ocularnetv2.1/dataprocessing/shape_predictor_5_face_landmarks.dat")
    dlib_gen = dlibFaceRed(detector, landmarks)

    GC = genCrops(dlib_gen, dst_fldr)

    gz_files = glob(src_fldr + os.sep + '*.tar.gz')
    # gzf = gz_files[0]

    c = 0
    set = 0
    for gzf in tqdm(gz_files):
        if c %250 == 0:
            dst_fldr_set = dst_fldr + '_' + str(set)
            if not os.path.exists(dst_fldr_set):
                os.makedirs(dst_fldr_set)
            GC.dst_fldr = dst_fldr_set
            set += 1
        c += 1
        GC(gzf)

    # batches = genBatches(gz_files, nb_batch = 8)
    # with ThreadPoolExecutor(max_workers=8) as pool:
    #     for batch in tqdm(batches):
    #         x = list(pool.map(GC, batch))
#
# I = Image.open('/media/narsi/LargeData/MITEyeGaze/original/00002/frames/00000.jpg')
# I = np.uint8(I.resize((I.size[0]//4, I.size[1]//4)).convert('RGB'))
#
# dets = detector(I.copy(), 0)
# rects = dlib.rectangles()
# for d in dets:
#     rects.extend([dlib.rectangle(left = d.rect.left()*4,
#                                  right = d.rect.right()*4,
#                                  top = d.rect.top()*4,
#                                  bottom = d.rect.bottom()*4)])
# rects
#
# shape = landmarks(I.copy(), rects[0])
# point_cloud = []
# for i in range(0, 5):
#     point_cloud.append([shape.part(i).y, shape.part(i).x, ])
# point_cloud = np.asarray(point_cloud)
