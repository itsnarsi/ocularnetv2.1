# @Author: Narsi Reddy <itsnarsi>
# @Date:   2018-07-01T19:23:35-05:00
# @Email:  sainarsireddy@outlook.com
# @Last modified by:   cibitaw1
# @Last modified time: 2018-07-11T19:02:46-05:00

import os
import numpy as np
import torch
import torch.nn as nn

class Net2(nn.Module):
    def __init__(self):
        super(Net2, self).__init__()
        self.conv_layers = nn.Sequential(
        self.conv_block(1, 16, 2),
        self.conv_block(16, 16, 1),
        nn.MaxPool2d(2), # 8X8
        nn.Dropout(0.25),
        self.conv_block(16, 32, 1),
        self.conv_block(32, 32, 1),
        nn.MaxPool2d(2),
        nn.Dropout(0.25),
        self.conv_block(32, 64, 1),
        self.conv_block(64, 64, 1),
        nn.Dropout(0.4)
        )

        self.dense_layers = nn.Sequential(
        nn.Linear(1024, 128),
        nn.Dropout(0.25),
        nn.ReLU(),
        nn.Linear(128, 2)
        )

    def conv_block(self, in_ch, out_ch, stride):
        block = nn.Sequential(nn.Conv2d(in_ch, out_ch, 3, stride, 1, bias=False),
        nn.BatchNorm2d(out_ch),
        nn.ReLU())

        return block

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(-1, 1024)
        x = self.dense_layers(x)
        return x

def initModel(weights_dir, cuda = False):
    model = torch.load(weights_dir, map_location=lambda storage, loc: storage)
    model.eval()
    if cuda:
        model.cuda()
    return model
