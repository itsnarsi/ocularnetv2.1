# @Author: Narsi Reddy <narsi>
# @Date:   2019-05-14T19:21:26-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-10-17T21:52:58-05:00
import os
import dlib
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import cv2
from tqdm import tqdm
from glob import glob

from PIL import Image

class dlibFaceRed(object):

    def __init__(self, detector, landmarks):

        self.detector = detector
        self.landmarks =  landmarks

    def __call__(self, img):
        point_cloud = []
        dets = self.detector(img, 0)

        if len(dets) == 0:
            return (False,[],[])
        rects = dlib.rectangles()
        rects.extend([d.rect for d in dets])
        shape = self.landmarks(img, rects[0])

        point_cloud = []
        for i in range(0, 5):
            point_cloud.append([shape.part(i).y, shape.part(i).x, ])
        point_cloud = np.asarray(point_cloud)

        bbox_re_ex = getBBOXExtended(point_cloud, a = [2, 3])
        bbox_le_ex = getBBOXExtended(point_cloud, a = [1, 0])


        return (True, bbox_re_ex, bbox_le_ex)

def getBBOXExtended(point_cloud, a = [1, 0]):
    x_min = point_cloud[a[0], 0]
    y_min = point_cloud[a[0], 1]
    x_max = point_cloud[a[1], 0]
    y_max = point_cloud[a[1], 1]
    cx = 0.5 * (x_min + x_max)
    cy = 0.5 * (y_min + y_max)
    W = (y_max - y_min) * 2.5
    H = W
    bbox = [int(cx - H * 0.5), int(cx + H * 0.5), int(cy - W * 0.5), int(cy + W * 0.5)]

    return bbox

def detectAndExtractEYE(img, dlib_gen, dst_fldr):
    try:
        I = Image.open(img).convert('RGB')

        I1 = I.resize((I.size[0]//3, I.size[1]//3), Image.BICUBIC)

        detect, bbox_re_ex, bbox_le_ex = dlib_gen(np.uint8(I1.convert('L')).copy())

        if detect is True:

            bbox_re_ex = [i * 3 for i in bbox_re_ex]
            bbox_le_ex = [i * 3 for i in bbox_le_ex]

            right_eye = img.split(os.sep)[-1].replace('.ppm', '_R.png').split('_')
            right_eye = right_eye[0] + 'R_' + '_'.join(right_eye[1:])
            left_eye = img.split(os.sep)[-1].replace('.ppm', '_L.png').split('_')
            left_eye = left_eye[0] + 'L_' + '_'.join(left_eye[1:])

            I.crop([bbox_re_ex[2], bbox_re_ex[0], bbox_re_ex[3], bbox_re_ex[1]]).resize((216, 216), Image.BICUBIC).transpose(Image.FLIP_LEFT_RIGHT).save(os.path.join(dst_fldr, right_eye))
            I.crop([bbox_le_ex[2], bbox_le_ex[0], bbox_le_ex[3], bbox_le_ex[1]]).resize((216, 216), Image.BICUBIC).save(os.path.join(dst_fldr, left_eye))
    except:
        pass

if __name__ == '__main__':

    src_fldr = '/media/narsi/LargeData/FERET/FACE/'
    dst_fldr = '/media/narsi/LargeData/FERET/EYECROPS'


    imgs = glob(src_fldr + os.sep + '*' + os.sep + '*_fa*.ppm')
    len(imgs)
    imgs += glob(src_fldr + os.sep + '*' + os.sep + '*_fb*.ppm')
    len(imgs)
    imgs += glob(src_fldr + os.sep + '*' + os.sep + '*_rb*.ppm')
    len(imgs)
    imgs += glob(src_fldr + os.sep + '*' + os.sep + '*_rc*.ppm')
    len(imgs)

    detector = dlib.cnn_face_detection_model_v1("/home/narsi/local/ocularnetv2.1/dataprocessing/mmod_human_face_detector.dat")
    landmarks =  dlib.shape_predictor("/home/narsi/local/ocularnetv2.1/dataprocessing/shape_predictor_5_face_landmarks.dat")

    dlib_gen = dlibFaceRed(detector, landmarks)

    for img in tqdm(imgs):
        detectAndExtractEYE(img, dlib_gen, dst_fldr)
