# @Author: Narsi Reddy <cibitaw1>
# @Date:   2019-09-18T15:12:54-05:00
# @Email:  sainarsireddy@outlook.com
# @Last modified by:   cibitaw1
# @Last modified time: 2019-09-18T17:18:18-05:00
import os
from PIL import Image
from glob import glob
from tqdm import tqdm
import pandas as pd
import numpy as np












imgs = glob('/media/cibitaw1/datasets/OcularNetV2/MITGaze/imgs/*/*.png')

imgs[0]

imgs_pars = []
for img in tqdm(imgs):
    sub_id = img.split(os.sep)[-1].split('_')[0]
    imgs_pars.append([img, sub_id])
imgs_pars = np.asarray(imgs_pars, dtype = np.object)

unique_subs = np.unique(imgs_pars[:, 1])

x = imgs_pars[imgs_pars[:, 1] == unique_subs[0], 0]

for usub in tqdm(list(unique_subs)):
    x = imgs_pars[imgs_pars[:, 1] == usub, 0]
    if len(x) >= 15:
        s = len(x)//15
        for i in range(0, len(x), s):
            end = i+s if len(x) > i+s else len(x)
            ids = np.random.randint(i, end, [1,])[0]
            # print('================================')
            # print(x[ids])
            # print('================================')
            for j in range(i, end):
                if ids != j:
                    os.remove(x[j])
