# @Author: Narsi Reddy <narsi>
# @Date:   2018-10-27T20:52:00-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-07-12T19:33:23-05:00
import os
import numpy as np
from PIL import Image
from skimage.transform import resize
from tqdm import tqdm
import torch
from eyeDetector import initModel, Net2
from glob import glob
import shutil


def dlibDetectFace(eyeCropChecker, img):
    I = np.asarray(Image.open(img).convert('L'))
    I = resize(I, (32, 32), order=3, mode='reflect')
    I = I - np.mean(I)
    I = I/(np.std(I)+0.0001)
    I = np.float32(np.expand_dims(np.expand_dims(I, 0), 0))
    with torch.no_grad():
        score = np.squeeze(eyeCropChecker(torch.from_numpy(I)).cpu().numpy())
    score = np.exp(score)/np.sum(np.exp(score))
    return score[0]>=0.95


wrong_crops_dir = '/media/cibitaw1/DATA/MITEyeGaze/eye_crops_WC'
for i in range(5):
    img_src_fldr = '/media/cibitaw1/DATA/MITEyeGaze/eye_crops_' + str(i)

    imgs = glob(img_src_fldr + os.sep + '*.png')

    eyeCropChecker = initModel(os.path.join(os.path.dirname( __file__), "eye_crop_check.pth.tar"))

    for img in tqdm(imgs):
        try:
            score = dlibDetectFace(eyeCropChecker, img)
            if not score:
                new_img = img.replace(img_src_fldr, wrong_crops_dir)
                if not os.path.exists(os.path.split(new_img)[0]):
                    os.makedirs(os.path.split(new_img)[0])
                shutil.move(img, new_img)
        except:
            new_img = img.replace(img_src_fldr, wrong_crops_dir)
            if not os.path.exists(os.path.split(new_img)[0]):
                os.makedirs(os.path.split(new_img)[0])
            shutil.move(img, new_img)
