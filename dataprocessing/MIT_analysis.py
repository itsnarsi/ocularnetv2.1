# @Author: Narsi Reddy <cibitaw1>
# @Date:   2019-07-09T20:07:05-05:00
# @Email:  sainarsireddy@outlook.com
# @Last modified by:   cibitaw1
# @Last modified time: 2019-07-13T11:57:05-05:00
import os
from glob import glob
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

def getPars(img):
    I = np.uint8(Image.open(img).convert('L'))
    sub_name = img.split(os.sep)[-1]
    return [sub_name, I.shape[0]]

def genBatches(files, nb_batch = 1024):
    batches = []
    batch = []
    for i in range(len(files)):
        batch.append(files[i])
        if len(batch) == nb_batch:
            batches.append(batch)
            batch = []
    if len(batch) > 0:
        batches.append(batch)
    return batches


img_pars = []
for i in range(5):
    src_fldr = '/media/cibitaw1/DATA/MITEyeGaze/eye_crops_' + str(i)

    imgs = glob(src_fldr + os.sep + '*.png')

    batches = genBatches(imgs, nb_batch = 16)
    with ProcessPoolExecutor(max_workers=16) as pool:
        for batch in tqdm(batches):
            img_pars += list(pool.map(getPars, batch))

img_pars = np.asarray(img_pars, dtype=str)
np.save('/media/cibitaw1/DATA/MITEyeGaze/img_anlaysis.npy', img_pars)

# img_pars = []
# for img in tqdm(imgs):
#     I = np.uint8(Image.open(img).convert('L'))
#     sub_name = img.split(os.sep)[-1].split('_')[0]
#     img_pars.append([sub_name, I.shape[0]])
