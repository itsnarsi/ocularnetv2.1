# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:05:22-05:00
# @Last modified by:   cibitaw1
# @Last modified time: 2019-08-01T10:56:20-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer, non_trainable_layer
from twrap.transform import ToZNorm, RandomBlur, RandomNoise
from utils.trainer_siamese import fit_model
from utils.models import *
from utils.datasets2 import dataset_visob, SiameseBatch

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

logging_dir = '/media/cibitaw1/DATA/SUMMER2019/OCULARNETV2.1/SMEX01'
model_instance_name = 'SIAMESE_NET_V1__mobilenetv2_E1__VISOB.S1.ALL_EXP02'

batch_size = 64
nb_epochs = 150
base_lr = 1e-4

transform=transforms.Compose([
    transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5),
    transforms.Grayscale(),
    RandomBlur(min_scale = 0.0, max_scale = 1.5),
    transforms.RandomRotation(20),
    transforms.Resize(180),
    transforms.RandomResizedCrop(size=160, scale=(0.85, 1.1), ratio=(0.95, 1.05)),
    # transforms.RandomCrop(128),
    # ToZNorm()
    transforms.ToTensor(),
    # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])


img_fldrs = ['/media/cibitaw1/datasets/OcularNetV2/visob/imgs/img_pars.csv']

train_dataset = dataset_visob(img_fldrs, transform=transform, visit = None, set_200 = False, session1 = True)
# Training data information
batch_sampler = SiameseBatch(train_dataset.img_pars[:, 1].copy(), batch_size=batch_size, nb_pos = 20, nb_neg = 20)
train_data_loader = DataLoader(train_dataset, batch_size=1,
                               shuffle=False, num_workers=8,
                               pin_memory=True, batch_sampler = batch_sampler)
train_batches = len(train_data_loader)

feat_model = mobilenetv2_E1(train_dataset.num_targets, classify = False, attention = True, illum_inv = True)
check_point_file = '/media/cibitaw1/DATA/SUMMER2019/OCULARNETV2.1/LFEX01/weights/mobilenetv2_E1_VISOBALLS1_EXP01/model_best.pth.tar'
checkpoint = torch.load(check_point_file, map_location=lambda storage, loc: storage)
feat_model.load_state_dict(checkpoint['state_dict'])
model = SIAMESE_NET_V1(feat_model = feat_model)
non_trainable_layer(model.LF)
non_trainable_layer(model.FM)

optimizer = torch.optim.Adam(parfilter(model), lr=base_lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=1e-5, amsgrad=False)

model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = nn.CrossEntropyLoss(), batch_size = batch_size,
                  num_epochs = nb_epochs, scheduler_steps = [125],
                  log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = False)
