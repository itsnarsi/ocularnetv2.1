# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-26T21:39:11-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-08-07T12:06:21-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer
from twrap.transform import ToZNorm, RandomBlur, RandomNoise
from utils.trainer_sphere_xa import fit_model
from utils.models import *
from utils.datasets import dataset, dataset_visob, dataset_dtd
from utils.metrics import TotalLMGM, SphereLoss_localNon3
from utils.lr_finder import LRFinder, CyclicLR

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

logging_dir = '/media/cibitaw1/DATA/SUMMER2019/OCULARNETV2.1/LFEX01'
model_instance_name = 'MODEL8_VISOB.400.SUB_EXP01'

batch_size = 64
nb_epochs = 500
base_lr = 1e-2

transform=transforms.Compose([
    transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5),
    transforms.Grayscale(),
    RandomBlur(min_scale = 0.0, max_scale = 1.5),
    transforms.RandomRotation(20),
    transforms.Resize(180),
    transforms.RandomResizedCrop(size=160, scale=(0.80, 1.2), ratio=(0.90, 1.1)),
    # transforms.RandomCrop(128),
    # ToZNorm()
    transforms.ToTensor(),
    # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])


# img_fldrs = ['/media/cibitaw1/datasets/OcularNetV2/MITGaze/img_pars.csv']
# train_dataset = dataset(img_fldrs, transform=transform)

img_fldrs = ['/media/cibitaw1/datasets/OcularNetV2/visob/imgs/img_pars.csv']
train_dataset = dataset_visob(img_fldrs, transform=transform, visit = 1, set_200 = True, session1 = False)

# Training data information
train_batches = len(train_dataset)//batch_size
train_data_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True)

print(train_dataset.num_targets)

model = MODEL8(train_dataset.num_targets, classify = False, attention = True, illum_inv = True)
model_summary([1, 160, 160], model)

max_lr = 0.01

optimizer = torch.optim.SGD(parfilter(model), base_lr, momentum=0.9, weight_decay=0.0001)
criterion = SphereLoss_localNon3(in_feats=model.embed_feat, n_classes=train_dataset.num_targets, scale = 25).cuda()
optimizer_loss = torch.optim.SGD(parfilter(criterion), 0.01, momentum=0.9, weight_decay=0.0001)

# criterion.use_s_cos = False
# lr_finder = LRFinder(model, optimizer, criterion, device="cuda")
# lr_finder.range_test(train_data_loader, end_lr=100, num_iter=500)
# lr_finder.plot()
# exit()


scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[150, 300], gamma=0.1)#CyclicLR(optimizer, base_lr=base_lr, max_lr=max_lr, step_size=train_batches*100)
scheduler_loss = torch.optim.lr_scheduler.MultiStepLR(optimizer_loss, milestones=[150, 300], gamma=0.1)

model = fit_model(model, train_data_loader, train_batches,
                  optimizer = optimizer, optimizer_loss = optimizer_loss,
                  criterion = criterion, batch_size = batch_size,
                  num_epochs = nb_epochs, init_epoch = 1,
                  scheduler = scheduler, scheduler_loss = scheduler_loss,
                  log_dir = logging_dir, log_instance = model_instance_name,
                  resume_train = False)
