# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-27T00:02:40-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-09T22:38:57-05:00
import os
import numpy as np
from PIL import Image
from tqdm import tqdm

import pandas as pd

# Pytorch
import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
import torchvision.transforms as transforms

from twrap.utils import model_summary, tensor_to_torch_var
from utils.models import *
from torch.utils.data import DataLoader
from utils.datasets import dataset, test_dataset_visob, TestSampler, test_dataset
from twrap.transform import ToZNorm

from twrap.utils import genROC

from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 12})

from glob import glob


X = np.load('/home/narsi/Downloads/img_anlaysis.npy')

sub_ids = X[:, 0]
widths = np.uint64(X[:, 1])

unique_subs = np.unique(sub_ids)

nb_samples = []
for us in list(unique_subs):
    nb_samples.append([us[:-1], us, np.sum(widths[sub_ids == us] >= 75)])

nb_samples = np.asarray(nb_samples, dtype= np.object)

new_set = []
for us in list(np.unique(nb_samples[:, 0])):
    new_set.append([us, np.mean(nb_samples[nb_samples[:, 0] == us, -1] >= 1000)])

new_set = np.asarray(new_set, dtype=np.object)

len(new_set[new_set[:, 1] == 1.0, 0])
sub_300 = new_set[new_set[:, 1] == 1.0, 0]
sub_600 = new_set[new_set[:, 1] == 1.0, 0]
sub_1000 = new_set[new_set[:, 1] == 1.0, 0]

np.savez('MIT_Gaze_subIDS.npz', sub_300, sub_600, sub_1000)

(transform=transforms.Compose([
    transforms.Grayscale(),
    transforms.Resize(144),
    transforms.CenterCrop(128),
    ToZNorm()
    ])


weights_dir = '/media/narsi/LargeData/SP2019/ocularnetv2.1/SPHERE/weights'

scores_list = [['device', 'visit', 'light_enroll', 'light_verify', 'eye', 'AUC', 'D-prime', 'EER', 'GMR@0.01FMR', 'GMR@0.001FMR']]

model_instance_name = 'PSNet1_SPH_VISOB_C128_S200'#
output_instance_name = 'PSNet1_SPH_VISOB_C128_S200' ##


model = PSNet1(400, classify = False)
check_point_file = weights_dir+ os.sep +model_instance_name + '/model_best.pth.tar'
checkpoint = torch.load(check_point_file, map_location=lambda storage, loc: storage)
model.load_state_dict(checkpoint['state_dict'])
model.cuda()
model.eval()
print('.')

dst_fldr = '/home/narsi/Gsync/LabWorks/2019/SPRING2019/unSupi/init_experiments_journal/'+output_instance_name
for device in ['IPHONE', 'OPPO', 'NOTE4']:#
    for visit in ['EC_VISIT_1','EC_VISIT_2']:#
        for light_enroll in ['office', 'daylight', 'dark']:#
            for light_verify in ['office', 'daylight','dark']:#
                for eye in ['l', 'r']:
                    if True:#light_enroll == light_verify:#

                        experiment_dst_fldr = dst_fldr + os.sep + device + os.sep + visit
                        if not os.path.exists(experiment_dst_fldr):
                            os.makedirs(experiment_dst_fldr)

                        src_fldr1 = '/media/narsi/fast_drive/CCRops/visob/imgs/'+visit+'/'+device+'/'+light_enroll
                        src_fldr2 = '/media/narsi/fast_drive/CCRops/visob/imgs/'+visit+'/'+device+'/'+light_verify

                        e_imgs = glob(src_fldr1 + os.sep + 'S1' + os.sep + '*_'+eye+'_*.png')
                        v_imgs = glob(src_fldr2 + os.sep + 'S2' + os.sep + '*_'+eye+'_*.png')

                        e_dataset = test_dataset_visob(e_imgs, transform)
                        v_dataset = test_dataset_visob(v_imgs, transform)

                        dataset_loader = DataLoader(e_dataset, num_workers=8, pin_memory=True, batch_sampler = TestSampler(len(e_dataset), 128))
                        e_feat = []
                        e_labels = []

                        for (batch_data, batch_target) in tqdm(dataset_loader):

                            input_var = tensor_to_torch_var(batch_data, True)
                            e_feat.append(model(input_var).data.cpu())
                            e_labels += batch_target
                            del input_var

                        e_feat = torch.cat(e_feat, dim = 0)
                        e_labels = np.asarray(e_labels, dtype = np.object)

                        dataset_loader = DataLoader(v_dataset, num_workers=8, pin_memory=True, batch_sampler = TestSampler(len(v_dataset), 128))
                        v_feat = []
                        v_labels = []

                        for (batch_data, batch_target) in tqdm(dataset_loader):

                            input_var = tensor_to_torch_var(batch_data, True)
                            v_feat.append(model(input_var).data.cpu())
                            v_labels += batch_target
                            del input_var

                        v_feat = torch.cat(v_feat, dim = 0)
                        v_labels = np.asarray(v_labels, dtype = np.object)


                        e_feat = e_feat.cuda()



                        targets = []
                        scores = []
                        unique_labels = list(np.unique(e_labels))

                        for i in tqdm(range(v_feat.shape[0])):

                            v_f = v_feat[i:i+1, ...].cuda()
                            v_l = v_labels[i]

                            s = F.cosine_similarity(e_feat, v_f).data.cpu().numpy()#(e_feat * v_f).sum(1).data.cpu().numpy()#
                            # s = torch.sum(torch.abs(e_feat - v_f), dim=1).data.cpu().numpy()
                            # s = torch.sum((e_feat != v_f).float(), dim=1).data.cpu().numpy()
                            t = 1 * (e_labels == v_l)
                            #
                            targets.append(t)
                            scores.append(s)

                            # for ul in unique_labels:
                            #     targets.append(1 * (ul == v_l))
                            #     scores.append(np.max(s[e_labels == ul]))

                        scores = np.asarray(scores, dtype = np.float64)
                        targets = np.asarray(targets, dtype = np.float64)

                        TAR, FAR, TH, EER, EER_thresh, AUC, GMRS = genROC(targets, -1 * scores, resolution = 1000, gmrsat = [0.01, 0.001])

                        plt.figure(figsize = (20, 10))
                        plt.subplot(1, 2, 1)
                        plt.semilogx(FAR, TAR)
                        plt.title('AUC:' + '{0:0.3f}'.format(AUC) + ' EER:' + '{0:0.2f}'.format(EER*100.0) + ' GMR@0.01FMR:' + '{0:0.2f}'.format(GMRS[0]) + ' GMR@0.001FMR:' + '{0:0.2f}'.format(GMRS[1]))
                        plt.xlim([1e-4, 1])
                        plt.ylim([0, 1])
                        plt.grid(b=True, which='major', color='k', linestyle='-')
                        plt.grid(b=True, which='minor', color='b', linestyle='--')

                        h1, x1 = np.histogram(scores[targets == 1], bins = 100)
                        h2, x2 = np.histogram(scores[targets == 0], bins = 100)

                        dp = np.abs(np.mean(scores[targets == 1]) - np.mean(scores[targets == 0]))/(0.5*(np.var(scores[targets == 1]) + np.var(scores[targets == 0])))**0.5

                        h1 = h1/np.sum(h1)
                        h2 = h2/np.sum(h2)

                        plt.subplot(1, 2, 2)
                        plt.plot(x1[:-1], h1)
                        plt.plot(x2[:-1], h2)
                        plt.grid(b=True, which='major', color='k', linestyle='-')
                        plt.grid(b=True, which='minor', color='b', linestyle='--')
                        plt.title('D-prime: ' + str(dp))
                        plt.savefig(experiment_dst_fldr + '/'+eye+'.'+device+'.'+light_enroll+'.'+light_verify + '.png')
                        plt.close()

                        scores_list.append([device, visit, light_enroll, light_verify, AUC, dp, EER*100.0, GMRS[0], GMRS[1]])

# del model, checkpoint

pd.DataFrame(scores_list).to_csv(dst_fldr + '_SCORES.csv', index=False)
