# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-24T21:05:22-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-04-26T21:39:02-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer
from twrap.transform import ToZNorm
from utils.trainer_LMGM import fit_model
from utils.models import *
from utils.datasets import dataset
from utils.metrics import TotalLMGM

logging_dir = '/media/narsi/LargeData/SP2019/ocularnetv2.1/LMG'
model_instance_name = 'resnet50_FT_LMG'

batch_size = 64
nb_epochs = 50
base_lr = 0.001
alpha = 1.0

transform=transforms.Compose([
    transforms.Grayscale(),
    transforms.Resize(144),
    transforms.RandomCrop(128),
    ToZNorm()
    ])

img_fldrs = ['/media/narsi/fast_drive/CCRops/miche/img_pars_1.csv',
             '/media/narsi/fast_drive/CCRops/ubipr/img_pars_1.csv',
             '/media/narsi/fast_drive/CCRops/ubirisv2/img_pars_1.csv']

train_dataset = dataset(img_fldrs, transform=transform)
# Training data information
train_batches = len(train_dataset)//batch_size
train_data_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True)

model = resnet50(train_dataset.num_targets)
model_summary([3, 224, 224], model)

# non_trainable_model(model)
# trainable_layer(model.L7)

# specifyLR(model, {'classify':[100*base_lr, 0]}, {'classify':[200*base_lr, 0]})
optimizer = torch.optim.SGD(parfilter(model), base_lr, momentum=0.9, weight_decay=0.0001)

model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = TotalLMGM(train_dataset.num_targets, model.embed_feat, alpha), batch_size = batch_size,
              num_epochs = nb_epochs, scheduler_setps = None,
              log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = False)
del optimizer

trainable_model(model)

batch_size = 16
base_lr = 0.00005
nb_epochs = 70
optimizer = torch.optim.SGD(parfilter(model), base_lr, momentum=0.9, weight_decay=0.0001)

model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = TotalLMGM(train_dataset.num_targets, model.embed_feat, alpha), batch_size = batch_size,
              num_epochs = nb_epochs, init_epoch = 51, scheduler_setps = None,
              log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = False)
