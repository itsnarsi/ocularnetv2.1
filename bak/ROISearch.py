# @Author: Narsi Reddy <narsi>
# @Date:   2019-07-12T22:46:39-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-16T21:54:01-05:00
import os
import numpy as np
from PIL import Image
from tqdm import tqdm

import pandas as pd

# Pytorch
import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
import torchvision.transforms as transforms

from twrap.utils import model_summary, tensor_to_torch_var
from utils.models import *
from torch.utils.data import DataLoader
from utils.datasets import dataset, test_dataset_visob, TestSampler, test_dataset
from twrap.transform import ToZNorm

from twrap.utils import genROC

from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 12})

from glob import glob
from sklearn.metrics import roc_curve

from utils.model_utils import genRegionMask
weights_dir = '/media/narsi/LargeData/SP2019/ocularnetv2.1/EX01/weights'

def dprime(targets, scores):

    Zt = scores[targets==1]
    Ztm = np.mean(Zt)
    Ztv = np.var(Zt)

    Zn = scores[targets==0]
    Znm = np.mean(Zn)
    Znv = np.var(Zn)

    d_1 = np.abs(Znm - Ztm)
    d_2 = (0.5 * (Ztv + Znv))**0.5

    return d_1/d_2

transform=transforms.Compose([
    transforms.Grayscale(),
    transforms.Resize(180),
    transforms.CenterCrop(160),
#     ToZNorm()
    transforms.ToTensor()
    ])

model = MODEL4(400, classify = False, attention = True, illum_inv=True)
check_point_file = '/media/narsi/LargeData/SP2019/ocularnetv2.1/EX01/weights/MODEL4_NCA_EXP02/model_best.pth.tar'
checkpoint = torch.load(check_point_file, map_location=lambda storage, loc: storage)
model.load_state_dict(checkpoint['state_dict'])
model.cuda()
model.eval()
print('.')

device = 'IPHONE'
visit1 = 'EC_VISIT_1'
visit2 = 'EC_VISIT_1'
light_enroll = 'office'
light_verify = 'office'
eye = 'l'

src_fldr1 = '/media/narsi/fast_drive/CCRops/visob/imgs/'+visit1+'/'+device+'/'+light_enroll
src_fldr2 = '/media/narsi/fast_drive/CCRops/visob/imgs/'+visit2+'/'+device+'/'+light_verify

e_imgs = glob(src_fldr1 + os.sep + 'S1' + os.sep + '*_'+eye+'_*.png')
v_imgs = glob(src_fldr2 + os.sep + 'S2' + os.sep + '*_'+eye+'_*.png')

e_dataset = test_dataset_visob(e_imgs, transform)
v_dataset = test_dataset_visob(v_imgs, transform)

e_dl = DataLoader(e_dataset, num_workers=8, pin_memory=True, batch_sampler = TestSampler(len(e_dataset), 64))
v_dl = DataLoader(v_dataset, num_workers=8, pin_memory=True, batch_sampler = TestSampler(len(v_dataset), 64))

window_search = []
for x in range(3, 21):
    for y in range(3, 21):
        for ix in range(20-x):
            for jx in range(20-y):
                M = np.float32(np.zeros((1, 1, 1, 20, 20)))
                M[:, :, :, ix:ix+x, jx:jx+y] = 1.0
                M = np.reshape(M, (1, 1, 1, 400))
                M = torch.from_numpy(M).cuda()

                model.regions = M#(w = 20, s1 = 13, s2 = 7, s3 = 7, s4 = 2)
                model.regions.data.requires_grad = False

                e_feat = []
                e_labels = []
                for (batch_data, batch_target) in tqdm(e_dl):
                    input_var = tensor_to_torch_var(batch_data, True)
                    e_feat.append(model(input_var).data.cpu())
                    e_labels += batch_target
                    del input_var
                e_feat = torch.cat(e_feat, dim = 0)
                e_labels = np.asarray(e_labels, dtype = np.object)

                v_feat = []
                v_labels = []
                for (batch_data, batch_target) in tqdm(v_dl):
                    input_var = tensor_to_torch_var(batch_data, True)
                    v_feat.append(model(input_var).data.cpu())
                    v_labels += batch_target
                    del input_var
                v_feat = torch.cat(v_feat, dim = 0)
                v_labels = np.asarray(v_labels, dtype = np.object)

                e_feat = e_feat.cuda()

                scores = []
                targets = []
                for i in tqdm(range(v_feat.shape[0])):
                    v_f = v_feat[i:i+1, ...].cuda()
                    v_l = v_labels[i]

                    s = F.cosine_similarity(e_feat, v_f).data.cpu().numpy()
                    t = 1 * (e_labels == v_l)
                    scores.append(s)
                    targets.append(t)
                scores = np.concatenate(scores)
                targets = np.concatenate(targets)

                dp = dprime(targets, scores)

                w_ = [x, y, ix, jx, dp]

                window_search.append(w_)

                print(w_)
                del M, targets, scores, e_feat, e_labels, v_feat, v_labels


np.save('window_search_dPrime.npy', np.asarray(window_search))

X = np.load('window_search_dPrime.npy')

w = [7, 7]

X1 = X[1 * (1 * (X[:, 0] == w[0]) *1* (X[:, 1] == w[1])) == 1, :]

ids = np.argsort(X1[:, -1], axis=-1)

Z = np.zeros((20, 20))
for i in range(X1.shape[0]):
    Z[int(X1[i, 2]),int(X1[i, 3])] = X1[i, -1]

plt.imshow(Z)
