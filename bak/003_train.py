# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-26T21:39:11-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-08-25T21:29:56-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer, non_trainable_layer
from twrap.transform import ToZNorm, RandomBlur, RandomNoise
from utils.trainer_LMGM import fit_model
from utils.models import *
from utils.stn_models import *
from utils.datasets import dataset, dataset_visob, dataset_dtd
from utils.metrics import TotalLMGM, SphereLoss, ProxyNCA
from utils.lr_finder import LRFinder

torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

logging_dir = '/media/narsi/LargeData/SP2019/ocularnetv2.1/STN01'
model_instance_name = 'STNMODEL_1_VISOB.ALL.S1_EXP01'


batch_size = 96
nb_epochs = 500
base_lr = 1e-2

transform=transforms.Compose([
    transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5),
    transforms.Grayscale(),
    RandomBlur(min_scale = 0.0, max_scale = 1.5),
    transforms.RandomRotation(20),
    transforms.Resize(180),
    transforms.RandomResizedCrop(size=160, scale=(0.80, 1.2), ratio=(0.90, 1.1)),
    # transforms.RandomCrop(128),
    # ToZNorm()
    transforms.ToTensor(),
    # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ])



# img_fldrs = ['/media/narsi/fast_drive/CCRops/miche/img_pars_1.csv',
#              '/media/narsi/fast_drive/CCRops/ubipr/img_pars_1.csv',
#              '/media/narsi/fast_drive/CCRops/feret/img_pars.csv',
#              '/media/narsi/fast_drive/CCRops/ubirisv2/img_pars_1.csv',]
# #
# train_dataset = dataset(img_fldrs, transform=transform)

img_fldrs = ['/media/narsi/fast_drive/CCRops/visob/img_pars.csv']
train_dataset = dataset_visob(img_fldrs, transform=transform, visit = None, set_200 = False, session1 = True)

# Training data information
train_batches = len(train_dataset)//batch_size
train_data_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True)

model = STNMODEL_1(train_dataset.num_targets, classify = False, attention = True, illum_inv = True)
model_summary([1, 160, 160], model)

params = [{'params': parfilter(model.LF)},
          {'params': parfilter(model.LW)},
          {'params': parfilter(model.STN), 'weight_decay' : 0.0}
          ]

optimizer = torch.optim.SGD(params, base_lr, momentum=0.9, weight_decay=0.0001)
criterion = SphereLoss(in_feats=model.embed_feat, n_classes=train_dataset.num_targets, scale = 25).cuda()

# criterion.use_s_cos = False
# lr_finder = LRFinder(model, optimizer, criterion, device="cuda")
# lr_finder.range_test(train_data_loader, end_lr=100, num_iter=500)
# lr_finder.plot()
# exit()

model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = criterion, batch_size = batch_size,
                  num_epochs = nb_epochs, scheduler_steps = None,
                  log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = True)
