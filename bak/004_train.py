# @Author: Narsi Reddy <narsi>
# @Date:   2019-04-26T21:39:11-05:00
# @Last modified by:   narsi
# @Last modified time: 2019-07-09T20:36:22-05:00
import numpy as np

import torch
torch.manual_seed(29)
from torch import nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.nn.parallel
cudnn.benchmark = True
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from twrap.utils import model_summary, parfilter, specifyLR, non_trainable_model, trainable_model, trainable_layer
from twrap.transform import ToZNorm, RandomBlur, RandomNoise
from utils.trainer_TRIPLET import fit_model
from utils.models import *
from utils.datasets import dataset, dataset_visob
from utils.metrics import ProxyNCA
from utils.lr_finder import LRFinder

logging_dir = '/media/narsi/LargeData/SP2019/ocularnetv2.1/TRIPLET'
model_instance_name = 'MODEL4_EXP01'

batch_size = 128
nb_epochs = 500
base_lr = 1e-3
max_lr = 1e-4

transform=transforms.Compose([
    transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5),
    transforms.Grayscale(),
    RandomBlur(min_scale = 0.0, max_scale = 2.5),
    transforms.RandomRotation(20),
    transforms.Resize(144),
    transforms.RandomResizedCrop(size=128, scale=(0.80, 1.2), ratio=(0.95, 1.05)),
    # transforms.RandomCrop(128),
    # ToZNorm()
    transforms.ToTensor()
    ])

img_fldrs = ['/media/narsi/fast_drive/CCRops/miche/img_pars_1.csv',
             '/media/narsi/fast_drive/CCRops/ubipr/img_pars_1.csv',
             # '/media/narsi/fast_drive/CCRops/ubirisv2/img_pars_1.csv',
             '/media/narsi/fast_drive/CCRops/feret/img_pars.csv']
train_dataset = dataset(img_fldrs, transform=transform)

# img_fldrs = ['/media/narsi/fast_drive/CCRops/visob/img_pars.csv']
# train_dataset = dataset_visob(img_fldrs, transform=transform, visit = 1, set_200 = True)

# Training data information
train_batches = len(train_dataset)//batch_size
train_data_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True)

model = MODEL4(train_dataset.num_targets, classify = False, attention = True, illum_inv = True)
model_summary([1, 160, 160], model)


optimizer = torch.optim.SGD(parfilter(model), base_lr, momentum=0.9, weight_decay=0.0001)


criterion = ProxyNCA(nb_classes = train_dataset.num_targets, sz_embed = model.embed_feat).cuda()

# criterion.use_s_cos = False
# lr_finder = LRFinder(model, optimizer, criterion, device="cuda")
# lr_finder.range_test(train_data_loader, end_lr=100, num_iter=500)
# lr_finder.plot()
# exit()


model = fit_model(model, train_data_loader, train_batches, optimizer = optimizer, criterion = criterion, batch_size = batch_size,
              num_epochs = nb_epochs, scheduler = scheduler,
              log_dir = logging_dir, log_instance = model_instance_name, use_cuda = True, resume_train = False)
